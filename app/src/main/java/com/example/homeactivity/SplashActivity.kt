package com.example.homeactivity

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.animateddilog.ColorDialog
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.Util
import okhttp3.Headers
import retrofit2.Call


class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 500L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        getVersion()

    }

    var request: Request? = null
    private fun getVersion() {

        val call: Call<SplashModel> = RetrofitClient.getAPIInterface().getVersion()
        request = RetrofitRequest(call, object : ResponseListener<SplashModel?> {
            override fun onResponse(response: SplashModel?, headers: Headers?) {


                Log.d("Splash", "getVersion $response")
                Log.d("Splash", "getVersion ${response!!.statusCode}")

                if (response!!.statusCode == 200) {
                    Log.d("Splash", "getVersion ${response!!.result!!.latestVersion}")

                    val serCode = response!!.result!!.latestVersion!!.toInt()
                    val versionCode: Int = BuildConfig.VERSION_CODE
                    if (versionCode < serCode) {
                        showPopUp()
                    } else {
                        splashDismiss()
                    }


                } else {
                    Log.d("Splash", "error ")
                    Util.showCustomAlert(
                        this@SplashActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                    finish()
                }

            }

            override fun onError(status: Int, error: String?) {
                Log.d("Splash", "error " + error)
                Util.toast(this@SplashActivity, "Please try again")
                finish()

            }

            override fun onFailure(throwable: Throwable?) {
                Util.toast(this@SplashActivity, "Please try again")
                finish()
            }

        })
        request?.enqueue()
    }

    private fun showPopUp() {

        val dialog = ColorDialog(this@SplashActivity)
        dialog.setColor("#8ECB54")
        dialog.setAnimationEnable(true)
        dialog.setTitle(("UPDATE"))
        dialog.setContentText("Update Avilable Please Update !!!")
        dialog.setNegativeListener(
            "Update",
            object : ColorDialog.OnNegativeListener {
                override fun onClick(dialog: ColorDialog) {
                    dialog.dismiss()
                    try {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=$packageName")
                            )
                        )
                    } catch (e: ActivityNotFoundException) {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                            )
                        )
                    }
                }
            })
        dialog.setPositiveListener(
            ("Cancel"),
            object : ColorDialog.OnPositiveListener {
                override fun onClick(dialog: ColorDialog) {
                    dialog.dismiss()
                    finishAffinity()

                }
            }).show()
    }

    private fun splashDismiss() {

        Handler(Looper.getMainLooper()).postDelayed({
            val i = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(i)
            finish()
        }, SPLASH_TIME_OUT)


    }
}