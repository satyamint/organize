package com.example.homeactivity.user.userevent.quickMatchDetails.matchlist

import android.app.Activity
import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.R
import com.squareup.picasso.Picasso

class ImageViewPagerAdapter(
    val activity: Activity,
    val imagesList: ArrayList<String>
) :
    RecyclerView.Adapter<ImageViewPagerAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.viewpager_item, parent, false)
        )
    }

    override fun getItemCount(): Int = imagesList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Picasso.get()
            .load(imagesList.get(position))
            .into(holder.view.findViewById<ImageView>(R.id.img))

        holder.view.findViewById<ImageView>(R.id.img).setOnClickListener(){
            openDilogForFullScreen(it)
        }
//        holder.view.findViewById<AppCompatTextView>(R.id.pageNumber).text = "$position"
    }

    var alertDialog: AlertDialog? = null
    private fun openDilogForFullScreen(v: View) {


        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater: LayoutInflater = activity.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.fullscreen_img, null)
        dialogBuilder.setView(dialogView)

        val imageToshow = dialogView.findViewById<View>(R.id.imageToshow) as ImageView

        imageToshow.setImageDrawable((v as AppCompatImageView).drawable)
        val closeBtn = dialogView.findViewById<View>(R.id.closeBtn) as Button

        closeBtn.setOnClickListener {
            alertDialog?.dismiss()


        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }

}