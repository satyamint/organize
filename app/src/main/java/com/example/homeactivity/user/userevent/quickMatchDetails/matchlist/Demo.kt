package com.example.homeactivity.user.userevent.quickMatchDetails.matchlist

import android.view.View
import android.widget.ImageView
import androidx.viewpager2.widget.ViewPager2
import com.example.homeactivity.R

class Demo : ViewPager2.PageTransformer {
    override fun transformPage(view: View, position: Float) {

        val whole = view.findViewById<View>(R.id.img) as ImageView


        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            if (whole != null) {
                whole.translationX = 0f
            }
        } else if (position <= 0) { // [-1,0]
            // Use the default slide transition when moving to the left page
            doTrans(view, position, whole)
        } else if (position <= 1) { // (0,1]
            // Fade the page out.
            doTrans(view, position, whole)
        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            if (whole != null) {
                whole.translationX = 0f
            }
        }
    }

    private fun doTrans(view: View, position: Float, description: View) {
        if (position != 0f) {
            val translationX = view.width * position
            description.translationX = -translationX
        } else {
            description.translationX = 0f
        }
    }
}
