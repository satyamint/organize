package com.example.homeactivity.user.org


class OrgModel {
    var matchPaidType: Boolean = false
    var matchFreeType: Boolean = false
    var soloAvil: Boolean = false
    var duoAvil: Boolean = false
    var squadAvil: Boolean = false
    var name: String? = null
    var language1: String? = ""
    var language2: String? = ""
    var image: Int = 0
    var activeMatchCount: Int = 0
}
