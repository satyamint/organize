package com.example.homeactivity.user.userevent.quickMatchDetails.matchlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.MatchDetails
import com.example.homeactivity.user.userevent.quickMatchDetails.QuickMatchDetailsActivity

class QuickMatchPager : Fragment() {

    private var tvMatch: TextView? = null
    private var tvDate: TextView? = null
    private var tvTime: TextView? = null
    private var tvMapName: TextView? = null
    private var matchDetails: MatchDetails? = null
    private var coverImage: String? = null
    private var res1: String? = null
    private var res2: String? = null
    private var matchType: String? = null
    private var phoneNumber: String? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.quick_match_pager, container, false)
        val btnDetails = view.findViewById<TextView>(R.id.btnDetais)
        tvMatch = view.findViewById<Button>(R.id.tvMatch) as TextView
        tvDate = view.findViewById<Button>(R.id.tvDate) as TextView
        tvTime = view.findViewById<Button>(R.id.tvTime) as TextView
        tvMapName = view.findViewById<Button>(R.id.tvMapName) as TextView
        btnDetails.setOnClickListener {
            val intent = Intent(activity, QuickMatchDetailsActivity::class.java)
            intent.putExtra("match", matchDetails)
            intent.putExtra("coverImage", coverImage)
            intent.putExtra("res1", res1)
            intent.putExtra("res2", res2)
            intent.putExtra("matchType", matchType)
            intent.putExtra("phoneNumber", phoneNumber)
            startActivity(intent)
        }
//        3Mar2022-2:21:am
        val date = matchDetails!!.startDateTime!!.split("-")[0]
        val time = matchDetails!!.startDateTime!!.split("-")[1]

        tvDate!!.setText(date)
        tvTime!!.setText(time)
        tvMapName!!.setText(matchDetails!!.mapName)

        tvMatch!!.setText("Match "+matchDetails!!.matchSequence)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        matchDetails = (arguments?.getParcelable<MatchDetails>("match"))
        coverImage = (arguments?.getString("coverImage"))
        res1 = (arguments?.getString("res1"))
        res2 = (arguments?.getString("res2"))
        matchType = (arguments?.getString("matchType"))
        phoneNumber = (arguments?.getString("phoneNumber"))
    }


    companion object {

        fun newInstance(
            match: MatchDetails,
            coverImage: String?,
            res1: String?,
            res2: String?,
            matchType: String?,
            phoneNumber: String?
        ): QuickMatchPager {
            val args = Bundle()
            args.putParcelable("match", match)
            args.putString("coverImage", coverImage)
            args.putString("res1", res1)
            args.putString("res2", res2)
            args.putString("matchType", matchType)
            args.putString("phoneNumber", phoneNumber)
            val fragment = QuickMatchPager()
            fragment.arguments = args
            return fragment
        }

    }

}