package com.example.homeactivity.user.userevent.pageradapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.homeactivity.user.userevent.tournmentDetails.TournmentDetailsActivity
import com.example.homeactivity.user.userevent.tournmentDetails.MatchPager

class ScreenSlidePagerAdapter(
    fragment: TournmentDetailsActivity,
    val listOfPagerContents: List<Array<String>>,
    val mPageNumbers: Int
) : FragmentStateAdapter(fragment) {


    override fun getItemCount(): Int = mPageNumbers

    override fun createFragment(position: Int): Fragment {
        val fragment = MatchPager()

        return fragment
    }
}