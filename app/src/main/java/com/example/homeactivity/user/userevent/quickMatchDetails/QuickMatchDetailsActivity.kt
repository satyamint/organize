package com.example.homeactivity.user.userevent.quickMatchDetails

import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.MatchDetails
import com.example.homeactivity.user.userevent.pageradapter.HorizontalMarginItemDecoration
import com.example.homeactivity.user.userevent.quickMatchDetails.matchlist.ImageViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator


private var toolBarTitle: TextView? = null

//private var img: ImageView? = null
private var tvParentDesc: TextView? = null
private var tvMapName: TextView? = null
private var tvmatchType: TextView? = null
private var tvDate: TextView? = null
private var tvTime: TextView? = null
private var tvPhone: TextView? = null
private var vpImages: ViewPager2? = null
private var pager_tab_layout: TabLayout? = null
private var relBack: RelativeLayout? = null
private var rel_image: RelativeLayout? = null
private var matchDetails: MatchDetails? = null
private var coverImage: String? = null
private var res1: String? = null
private var res2: String? = null
private var matchType: String? = null
private var phoneNumber: String? = null

class QuickMatchDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_match_details)
//        toolBarTitle = findViewById<View>(R.id.tool_bar_title) as TextView
//        img = findViewById<View>(R.id.img) as ImageView
//        Glide.with(this).asGif().load(R.raw.bgmi1).into(img!!)
//
//        toolBarTitle!!.text = "Game Details"

        tvDate = findViewById(R.id.tvDate)
        tvTime = findViewById(R.id.tvTime)
        tvPhone = findViewById(R.id.tvPhone)
        tvParentDesc = findViewById(R.id.tvParentDesc)
        tvMapName = findViewById(R.id.tvMapName)
        tvmatchType = findViewById(R.id.tvmatchType)

        vpImages = findViewById<ViewPager2>(R.id.vpImages)
        pager_tab_layout = findViewById<TabLayout>(R.id.into_tab_layout)
        relBack = findViewById<RelativeLayout>(R.id.relBack)
        rel_image = findViewById<RelativeLayout>(R.id.rel_image)

        matchDetails = intent.getParcelableExtra<MatchDetails>("match")
        coverImage = intent.getStringExtra("coverImage")
        res1 = intent.getStringExtra("res1")
        res2 = intent.getStringExtra("res2")
        matchType = intent.getStringExtra("matchType")
        phoneNumber = intent.getStringExtra("phoneNumber")


        tvMapName!!.setText(matchDetails!!.mapName)
        tvParentDesc!!.setText(matchDetails!!.matchRulesDesc)
        tvmatchType!!.setText(matchType)
        tvDate!!.setText(matchDetails!!.startDateTime!!.split("-")[0])
        tvTime!!.setText(matchDetails!!.startDateTime!!.split("-")[1])
        tvPhone!!.setText(phoneNumber)
        relBack!!.setOnClickListener {
            finish()
        }


        val list = arrayListOf<String>()
        if (coverImage != null)
            list.add(coverImage!!)

        if (res1 != null)
            list.add(res1!!)

        if (res2 != null)
            list.add(res2!!)
        if (list.size == 0) {
            rel_image!!.visibility = View.GONE
        } else {
            rel_image!!.visibility = View.VISIBLE
        }

        vpImages!!.adapter = ImageViewPagerAdapter(this@QuickMatchDetailsActivity, list)

        TabLayoutMediator(pager_tab_layout!!, vpImages!!)
        { tab, position ->
            vpImages!!.setCurrentItem(tab.position, true)

        }.attach()

        vpImages!!.offscreenPageLimit = 1

// Add a PageTransformer that translates the next and previous items horizontally
// towards the center of the screen, which makes them visible
        val nextItemVisiblePx = resources.getDimension(R.dimen.viewpager_next_item_visible)
        val currentItemHorizontalMarginPx =
            resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position
            // Next line scales the item's height. You can remove it if you don't want this effect
            page.scaleY = 1 - (0.25f * Math.abs(position))
            // If you want a fading effect uncomment the next line:
            // page.alpha = 0.25f + (1 - abs(position))
        }
        vpImages!!.setPageTransformer(pageTransformer)

// The ItemDecoration gives the current (centered) item horizontal margin so that
// it doesn't occupy the whole screen width. Without it the items overlap
        val itemDecoration = applicationContext?.let {
            HorizontalMarginItemDecoration(
                it,
                R.dimen.viewpager_current_item_horizontal_margin
            )
        }
        if (itemDecoration != null) {
            vpImages!!.addItemDecoration(itemDecoration)
        }


    }

}