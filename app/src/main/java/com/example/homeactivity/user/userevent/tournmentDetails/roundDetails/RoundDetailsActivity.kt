package com.example.homeactivity.user.userevent.tournmentDetails.roundDetails

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.R
import com.example.homeactivity.user.userevent.tournmentDetails.matchDetails.TourMatchDetailsActivity

private var btnMore: Button? = null

class RoundDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_round_details)
        btnMore = findViewById<Button>(R.id.btnMoreDetais)

        btnMore!!.setOnClickListener {
            val intent = Intent(this@RoundDetailsActivity, TourMatchDetailsActivity::class.java)
            startActivity(intent)
        }
    }
}