package com.example.homeactivity.user.org

import com.google.gson.annotations.SerializedName


class FilterOrgModel {
    @SerializedName("statusCode")
    public var statusCode: Int = 0

    @SerializedName("status")
    public var status: Boolean = false

    @SerializedName("result")
    public var resultList: ArrayList<Result>? = null
}

public class Result() {
    @SerializedName("_id")
    public var _id: String? = null


    @SerializedName("name")
    public var name: String? = null

    @SerializedName("status")
    public var status: String? = null

    @SerializedName("profilePhoto")
    public var profilePhoto: String? = null

    @SerializedName("orgName")
    public var orgName: String? = null

    @SerializedName("gameMetadata")
    public var gameMetadata: GameMetadata? = null

}

public class GameMetadata() {
    @SerializedName("languages")
    public var languages: ArrayList<String>? = null

    @SerializedName("matchTypes")
    public var matchTypes: ArrayList<String>? = null

    @SerializedName("hasPaidEvents")
    public var hasPaidEvents: Boolean? = false

    @SerializedName("hasFreeEvents")
    public var hasFreeEvents: Boolean? = false

    @SerializedName("totalGameEvents")
    public var totalGameEvents: Int? = 0

    @SerializedName("_id")
    public var _id: String? = null

}