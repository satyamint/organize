package com.example.homeactivity.user.org

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminprofile.ProfileActivity
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.OnItemClickListener
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.user.userevent.quickMatchDetails.GamesListActivity
import com.example.homeactivity.utility.Util
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.floatingactionbutton.FloatingActionButton
import okhttp3.Headers
import retrofit2.Call
import java.util.*


private var drawerList: RecyclerView? = null
private var toolBarTitle: TextView? = null
private var tvFilterIndicator: TextView? = null
private var relSearch: RelativeLayout? = null
private var relBack: RelativeLayout? = null
private var searchLayout: SearchView? = null
private var relTool: RelativeLayout? = null
private var faFilter: FloatingActionButton? = null
private var progressBar: ProgressBar? = null

private val listItems = ArrayList<Result>()
private var adapter: OrgListAdapter? = null
private var filterList = ArrayList<String>()
private var cx = 0.0f
private var cy: Float = 0.0f
lateinit var mAdView1: AdView

class OrgListActivity : AppCompatActivity(), OnItemClickListener<Result> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_org_list)
        MobileAds.initialize(this) {}
        drawerList = findViewById<View>(R.id.left_drawer) as RecyclerView
        toolBarTitle = findViewById<View>(R.id.tool_bar_title) as TextView
        tvFilterIndicator = findViewById<View>(R.id.tvFilterIndicator) as TextView
        relSearch = findViewById<View>(R.id.relSearch) as RelativeLayout
        searchLayout = findViewById<View>(R.id.search_layout) as SearchView
        relTool = findViewById<View>(R.id.rel_tool) as RelativeLayout
        relBack = findViewById<View>(R.id.relBack) as RelativeLayout
        faFilter = findViewById<View>(R.id.faFilter) as FloatingActionButton
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        toolBarTitle!!.text = "Org List"
        progressBar!!.visibility = View.GONE

        listItems.clear()

        listAdapter()
        filterList.clear()
        filterSelectedIndication()
        getList()
        relSearch!!.visibility = View.VISIBLE
        relBack!!.setOnClickListener(View.OnClickListener {

            finish()
        })

        relSearch!!.setOnClickListener(View.OnClickListener {

            keyboardUp(searchLayout!!)
        })
        searchLayout!!.setOnCloseListener {

            adapter!!.refressList(listItems)
            keyboardDown(searchLayout!!)
        }

        searchLayout!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(qString: String): Boolean {

                if (adapter != null)
                    adapter!!.filter(qString, listItems)
                return false
            }

            override fun onQueryTextSubmit(qString: String): Boolean {
                return false
            }
        })
        faFilter!!.setOnClickListener {

            openDilog()
        }
    }

    private fun filterSelectedIndication() {
        if (filterList.size > 0)
            tvFilterIndicator!!.visibility = View.VISIBLE
        else
            tvFilterIndicator!!.visibility = View.INVISIBLE
    }

    var alertDialog: AlertDialog? = null
    private fun openDilog() {


        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.filter_popup, null)
        dialogBuilder.setView(dialogView)

        val tvPaid = dialogView.findViewById<View>(R.id.tvPaid) as TextView
        val tvFree = dialogView.findViewById<View>(R.id.tvFree) as TextView
        val spinnerLang = dialogView.findViewById<View>(R.id.spinner_lang) as Spinner

        val tvSolo = dialogView.findViewById<View>(R.id.tvSolo) as TextView
        val tvDuo = dialogView.findViewById<View>(R.id.tvDuo) as TextView
        val tvSquad = dialogView.findViewById<View>(R.id.tvSquad) as TextView


        val btnCancel = dialogView.findViewById<View>(R.id.btnCancel) as Button
        val btnOK = dialogView.findViewById<View>(R.id.btnOK) as Button

        /* spinnerLang?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
             override fun onNothingSelected(parent: AdapterView<*>?) {

             }

             override fun onItemSelected(
                 parent: AdapterView<*>?,
                 view: View?,
                 position: Int,
                 id: Long
             ) {
                 Toast.makeText(
                     this@OrgListActivity,
                     "Spinner " + "  ----  " + spinnerLang!!.selectedItem.toString(),
                     Toast.LENGTH_SHORT
                 ).show()
             }

         }*/


        if (filterList.contains("free")) {
            Util.setUtilBackGround(tvFree, this, R.drawable.bodor_selector_one)
        } else {
            Util.setUtilBackGround(tvFree, this, R.color.white)
        }
        if (filterList.contains("paid")) {
            Util.setUtilBackGround(tvPaid, this, R.drawable.bodor_selector_two)
        } else {
            Util.setUtilBackGround(tvPaid, this, R.color.white)
        }
        if (filterList.contains("solo")) {
            Util.setUtilBackGround(tvSolo, this, R.drawable.bodor_selector_three)
        } else {
            Util.setUtilBackGround(tvSolo, this, R.color.white)
        }

        if (filterList.contains("duo")) {
            Util.setUtilBackGround(tvDuo, this, R.drawable.bodor_selector_four)
        } else {
            Util.setUtilBackGround(tvDuo, this, R.color.white)
        }
        if (filterList.contains("squad")) {
            Util.setUtilBackGround(tvSquad, this, R.drawable.bodor_selector_five)
        } else {
            Util.setUtilBackGround(tvSquad, this, R.color.white)
        }

        tvPaid.setOnClickListener {

            if (filterList.contains("paid")) {
                filterList.remove("paid")
                Util.setUtilBackGround(tvPaid, this, R.color.white)
            } else {
                filterList.add("paid")
                Util.setUtilBackGround(tvPaid, this, R.drawable.bodor_selector_two)
            }

        }

        tvFree.setOnClickListener {

            if (filterList.contains("free")) {
                filterList.remove("free")
                Util.setUtilBackGround(tvFree, this, R.color.white)
            } else {
                filterList.add("free")
                Util.setUtilBackGround(tvFree, this, R.drawable.bodor_selector_one)
            }

        }
        tvSolo.setOnClickListener {

            if (filterList.contains("solo")) {
                filterList.remove("solo")
                Util.setUtilBackGround(tvSolo, this, R.color.white)
            } else {
                filterList.add("solo")
                Util.setUtilBackGround(tvSolo, this, R.drawable.bodor_selector_three)
            }

        }
        tvDuo.setOnClickListener {

            if (filterList.contains("duo")) {
                filterList.remove("duo")
                Util.setUtilBackGround(tvDuo, this, R.color.white)
            } else {
                filterList.add("duo")
                Util.setUtilBackGround(tvDuo, this, R.drawable.bodor_selector_four)
            }

        }
        tvSquad.setOnClickListener {

            if (filterList.contains("squad")) {
                filterList.remove("squad")
                Util.setUtilBackGround(tvSquad, this, R.color.white)
            } else {
                filterList.add("squad")
                Util.setUtilBackGround(tvSquad, this, R.drawable.bodor_selector_five)
            }

        }

        btnOK.setOnClickListener {
            alertDialog?.dismiss()
            filterSelectedIndication()
            if (adapter != null) {
                adapter!!.filter(filterList, listItems, spinnerLang!!.selectedItem.toString())
            }

        }
        btnCancel.setOnClickListener {
            alertDialog?.dismiss()
            filterList.clear()
            filterSelectedIndication()
            adapter!!.filter("", listItems)
        }
        alertDialog = dialogBuilder.create()
        alertDialog!!.setCancelable(false)
        alertDialog?.show()
    }


    private fun listAdapter() {
        drawerList?.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 1)
        drawerList?.layoutManager = layoutManager
        adapter = OrgListAdapter(this@OrgListActivity, this)
        drawerList?.adapter = adapter

        adapter!!.setList(listItems)

    }


    override fun onItemClick(t: Result, view: View, position: Int) {

//        Toast.makeText(this, "${t.name}", Toast.LENGTH_LONG).show()
        if (view.tag.equals("Profile")) {
            val intent = Intent(this@OrgListActivity, ProfileActivity::class.java)
            intent.putExtra("id", t._id)
            startActivity(intent)

        } else {

            val intent = Intent(this@OrgListActivity, GamesListActivity::class.java)
            intent.putExtra("id", t._id)
            startActivity(intent)

        }

    }

    override fun onBackPressed() {


        if (searchLayout!!.visibility == View.VISIBLE) {
            searchLayout!!.setQuery("", false)
            keyboardDown(searchLayout!!)
        } else {
            super.onBackPressed()
        }
    }

    var request: Request? = null
    private fun getList() {

        progressBar!!.visibility = View.VISIBLE
        val call: Call<FilterOrgModel> = RetrofitClient.getAPIInterface()
            .getOrgList("VERIFIED", "id name status orgName profilePhoto gameMetadata")
        request = RetrofitRequest(call, object : ResponseListener<FilterOrgModel?> {
            override fun onResponse(response: FilterOrgModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("getList", "response " + response)

                if (response!!.statusCode == 200) {

                    listItems.clear()
                    listItems.addAll(response!!.resultList!!)

                    adapter!!.setList(listItems)
                } else {
                    Log.d("getList", "error ")
                    Util.showCustomAlert(
                        this@OrgListActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("getList", "error " + error)
                Util.showCustomAlert(
                    this@OrgListActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }


    private fun keyboardDown(v2: SearchView): Boolean {
        if (cx == 0f) cx =
            relSearch!!.getX() + relSearch!!.getWidth() / 2

        if (cy == 0f) cy =
            relSearch!!.getY() + relSearch!!.getWidth() / 2

        // get the final radius for the clipping circle
//        int finalRadius = Math.max(v2.getWidth(), v2.getHeight())/2;

        // get the final radius for the clipping circle
//        int finalRadius = Math.max(v2.getWidth(), v2.getHeight())/2;
        val initialRadius = Math.hypot(cx.toDouble(), cy.toDouble()).toInt()
        // create the animator for this view (the start radius is zero)
        // create the animator for this view (the start radius is zero)
        val anim: Animator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(
                v2,
                cx.toInt(),
                cy.toInt(),
                initialRadius.toFloat(),
                0f
            )


            // make the view visible and start the animation


            // make the view invisible when the animation is done
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    v2.setVisibility(View.GONE)
                }
            })
            anim.start()
        } else {
            v2.setVisibility(View.GONE)
        }

        relSearch!!.setEnabled(true)


        return false
    }

    fun keyboardUp(v2: View) {
        relSearch?.setEnabled(true)
        v2.visibility = View.VISIBLE
        searchLayout!!.setIconified(false)
        cx = relSearch!!.getX() + relSearch!!.getWidth() / 2
        cy = relSearch!!.getY() + relSearch!!.getWidth() / 2

        // get the final radius for the clipping circle
        val endRadius = Math.hypot(cx.toDouble(), cy.toDouble()).toInt()
        // create the animator for this view (the start radius is zero)
        val anim: Animator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(
                v2,
                cx.toInt(),
                cy.toInt(),
                0f,
                endRadius.toFloat()
            )


            // make the view visible and start the animation
            anim.start()
        } else {
        }
    }

}