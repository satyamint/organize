package com.example.homeactivity.user.userevent.quickMatchDetails.matchlist

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.ContactDetails
import com.example.homeactivity.admin.event.Result
import com.example.homeactivity.user.userevent.pageradapter.HorizontalMarginItemDecoration
import com.example.homeactivity.user.userevent.pageradapter.QuickMatchScreenSlidePagerAdapter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator


private var viewPager2: ViewPager2? = null
private var tvResult: TextView? = null
private var tvDesc: TextView? = null
private var tvmatchType: TextView? = null
private var tvPhone: TextView? = null
private var tvPaid: TextView? = null
private var relYtube: RelativeLayout? = null
private var vpImages: ViewPager2? = null
private var pager_tab_layout: TabLayout? = null
private var linReg: LinearLayout? = null
private var relBack: RelativeLayout? = null
private var rel_image: RelativeLayout? = null
var alertDialog: AlertDialog? = null
private var finalResultobj: Result? = null
lateinit var mAdView1: AdView

class QuickMatchListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_match_list)
        MobileAds.initialize(this) {}
        viewPager2 = findViewById(R.id.intro_pager)
        tvResult = findViewById(R.id.tvResult)
        tvPhone = findViewById(R.id.tvPhone)
        relYtube = findViewById(R.id.relYtube)
        tvDesc = findViewById(R.id.tvDesc)
        linReg = findViewById(R.id.linReg)
        rel_image = findViewById(R.id.rel_image)
        tvPaid = findViewById(R.id.tvPaid)
        relBack = findViewById(R.id.relBack)
        tvmatchType = findViewById(R.id.tvmatchType)
        vpImages = findViewById<ViewPager2>(R.id.vpImages)
        pager_tab_layout = findViewById<TabLayout>(R.id.into_tab_layout)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)

        val onOffSwitch = findViewById<View>(R.id.swResult) as Switch

        finalResultobj = intent.getParcelableExtra<Result>("Resultobj")
        finalResultobj!!.contactDetails =
            intent.getParcelableExtra<ContactDetails>("ContactDetails")
        finalResultobj!!.matchList = intent.getParcelableArrayListExtra("listResult")


        tvResult!!.visibility = View.GONE
        onOffSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tvResult!!.visibility = View.VISIBLE
            } else {
                tvResult!!.visibility = View.GONE
            }

        }

        tvmatchType!!.setText(finalResultobj!!.matchType)
        tvResult!!.setText(finalResultobj!!.result)
        tvDesc!!.setText(finalResultobj!!.rules)

        if (finalResultobj!!.isPaid!!) {
            tvPaid!!.setText("Paid")
        } else {
            tvPaid!!.setText("Free")
        }


        tvDesc!!.setMovementMethod(LinkMovementMethod.getInstance());

        val list = arrayListOf<String>()
        if (finalResultobj!!.coverImage != null)
            list.add(finalResultobj!!.coverImage!!)

        if (finalResultobj!!.resultImage1 != null)
            list.add(finalResultobj!!.resultImage1!!)

        if (finalResultobj!!.resultImage2 != null)
            list.add(finalResultobj!!.resultImage2!!)

        if (list.size == 0) {
            rel_image!!.visibility = View.GONE
        } else {
            rel_image!!.visibility = View.VISIBLE
        }

        vpImages!!.adapter = ImageViewPagerAdapter(this@QuickMatchListActivity, list)

        if (finalResultobj!!.youtubeLink.isNullOrEmpty()) {
            relYtube!!.visibility = View.GONE
        } else {
            relYtube!!.visibility = View.VISIBLE
        }

        relYtube!!.setOnClickListener {

            openYoutube(finalResultobj!!.youtubeLink)
        }
        linReg!!.setOnClickListener {
            openDilog()

        }
        relBack!!.setOnClickListener {
            finish()
        }
        tvPhone!!.setText("" + finalResultobj!!.contactDetails!!.mobileNumber);

        val pagerAdapter =
            QuickMatchScreenSlidePagerAdapter(
                this,
                finalResultobj!!.matchList,
                finalResultobj!!.coverImage,
                finalResultobj!!.resultImage1,
                finalResultobj!!.resultImage2,
                finalResultobj!!.matchType,
                finalResultobj!!.contactDetails!!.mobileNumber

            )



        viewPager2!!.adapter = pagerAdapter
        TabLayoutMediator(pager_tab_layout!!, vpImages!!)
        { tab, position ->
            vpImages!!.setCurrentItem(tab.position, true)

        }.attach()

//        vpImages!!.setPageTransformer(Demo())

        // You need to retain one page on each side so that the next and previous items are visible
        viewPager2!!.offscreenPageLimit = 1

// Add a PageTransformer that translates the next and previous items horizontally
// towards the center of the screen, which makes them visible
        val nextItemVisiblePx = resources.getDimension(R.dimen.viewpager_next_item_visible)
        val currentItemHorizontalMarginPx =
            resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position
            // Next line scales the item's height. You can remove it if you don't want this effect
            page.scaleY = 1 - (0.25f * Math.abs(position))
            // If you want a fading effect uncomment the next line:
            // page.alpha = 0.25f + (1 - abs(position))
        }
        viewPager2!!.setPageTransformer(pageTransformer)

// The ItemDecoration gives the current (centered) item horizontal margin so that
// it doesn't occupy the whole screen width. Without it the items overlap
        val itemDecoration = applicationContext?.let {
            HorizontalMarginItemDecoration(
                it,
                R.dimen.viewpager_current_item_horizontal_margin
            )
        }
        if (itemDecoration != null) {
            viewPager2!!.addItemDecoration(itemDecoration)
        }


    }

    private fun openYoutube(youtubeLink: String?) {

        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://$youtubeLink"))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {

            val i =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://youtube.com/watch?v=$youtubeLink"))
            startActivity(i)
        }
    }

    fun getListOfPagerContents(): List<String> {

        val list = listOf("Match 1", "Match 2", "Match 3")
//        val list = listOf<String>()

        return list
    }

    private fun openDilog() {

        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.create_reg_pop_up, null)
        dialogBuilder.setView(dialogView)
        val idOk = dialogView.findViewById<TextView>(R.id.idok)

        idOk.setOnClickListener {
            alertDialog!!.dismiss()
        }

        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }

}