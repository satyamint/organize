package com.example.homeactivity.user.userevent.tournmentDetails

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.homeactivity.R
import com.example.homeactivity.user.userevent.pageradapter.HorizontalMarginItemDecoration
import com.example.homeactivity.user.userevent.pageradapter.ScreenSlidePagerAdapter

private var viewPager2: ViewPager2? = null
private val mPageNumbers: Int = 5

class TournmentDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tournment_details)
        viewPager2 = findViewById(R.id.intro_pager)

        val pagerAdapter =
            ScreenSlidePagerAdapter(this, getListOfPagerContents(), mPageNumbers)
        viewPager2!!.adapter = pagerAdapter




        // You need to retain one page on each side so that the next and previous items are visible
        viewPager2!!.offscreenPageLimit = 1

// Add a PageTransformer that translates the next and previous items horizontally
// towards the center of the screen, which makes them visible
        val nextItemVisiblePx = resources.getDimension(R.dimen.viewpager_next_item_visible)
        val currentItemHorizontalMarginPx =
            resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position
            // Next line scales the item's height. You can remove it if you don't want this effect
            page.scaleY = 1 - (0.25f * Math.abs(position))
            // If you want a fading effect uncomment the next line:
            // page.alpha = 0.25f + (1 - abs(position))
        }
        viewPager2!!.setPageTransformer(pageTransformer)

// The ItemDecoration gives the current (centered) item horizontal margin so that
// it doesn't occupy the whole screen width. Without it the items overlap
        val itemDecoration = applicationContext?.let {
            HorizontalMarginItemDecoration(
                it,
                R.dimen.viewpager_current_item_horizontal_margin
            )
        }
        if (itemDecoration != null) {
            viewPager2!!.addItemDecoration(itemDecoration)
        }

    }

    fun getListOfPagerContents(): List<Array<String>> {

        val ar1 = arrayOf("", "", "R")
        val ar2 = arrayOf("", "", "G")
        return listOf(ar1, ar2)
    }
}