package com.example.homeactivity.user.userevent.quickMatchDetails

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.CreateEventListAdapter
import com.example.homeactivity.admin.event.EventModel
import com.example.homeactivity.admin.event.Result
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.OnItemClickListener
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.user.userevent.quickMatchDetails.matchlist.QuickMatchListActivity
import com.example.homeactivity.utility.Util
import okhttp3.Headers
import retrofit2.Call
import java.util.*


private var drawerList: RecyclerView? = null
private var toolBarTitle: TextView? = null
private val listItems = ArrayList<Result>()
private var adapter: CreateEventListAdapter? = null
private var relBack: RelativeLayout? = null
private var progressBar: ProgressBar? = null

class GamesListActivity : AppCompatActivity(), OnItemClickListener<Result> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_list)
        drawerList = findViewById<View>(R.id.left_drawer) as RecyclerView
        toolBarTitle = findViewById<View>(R.id.tool_bar_title) as TextView
        relBack = findViewById(R.id.relBack)
        progressBar = findViewById(R.id.progressBar)

        toolBarTitle!!.text = "Game List"

        listAdapter()
        getList(intent.getStringExtra("id").toString())
        relBack!!.setOnClickListener() {
            finish()
        }

    }


    var request: Request? = null
    private fun getList(id: String) {

        progressBar!!.visibility = View.VISIBLE
        val call: Call<EventModel> = RetrofitClient.getAPIInterface()
            .getUserEventList(id)
        request = RetrofitRequest(call, object : ResponseListener<EventModel?> {
            override fun onResponse(response: EventModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("getList", "response " + response)

                if (response!!.statusCode == 200) {
                    listItems.clear()
                    listItems.addAll(response!!.resultList!!)
                    adapter!!.setList(listItems)

                } else {
                    Log.d("getList", "error ")
                    Util.showCustomAlert(
                        this@GamesListActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("getList", "error " + error)
                Util.toast(this@GamesListActivity, "Error ")
                finish()
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
                Util.toast(this@GamesListActivity, "Error ")
                finish()
            }

        })
        request?.enqueue()
    }


    private fun listAdapter() {
        drawerList?.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 1)
        drawerList?.layoutManager = layoutManager
        adapter = CreateEventListAdapter(this@GamesListActivity, this, false)
        drawerList?.adapter = adapter

        adapter!!.setList(listItems)

    }


    override fun onItemClick(t: Result, view: View, position: Int) {
        val intent = Intent(this@GamesListActivity, QuickMatchListActivity::class.java)
        intent.putExtra("Resultobj", t)
        intent.putExtra("ContactDetails", t.contactDetails)
        intent.putParcelableArrayListExtra("listResult", t.matchList)
        startActivity(intent)
    }


}