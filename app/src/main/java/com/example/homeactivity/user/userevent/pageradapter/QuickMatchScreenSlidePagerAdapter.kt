package com.example.homeactivity.user.userevent.pageradapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.homeactivity.admin.event.MatchDetails
import com.example.homeactivity.user.userevent.quickMatchDetails.matchlist.QuickMatchListActivity
import com.example.homeactivity.user.userevent.quickMatchDetails.matchlist.QuickMatchPager

class QuickMatchScreenSlidePagerAdapter(
    fragment: QuickMatchListActivity,
    val listOfPagerContents: ArrayList<MatchDetails>?,
    private val coverImage: String?,
    private val resultImage1: String?,
    private val resultImage2: String? ,
    private val matchType: String?,
    private val phoneNumber: String?
) : FragmentStateAdapter(fragment) {


    override fun getItemCount(): Int = listOfPagerContents!!.size

    override fun createFragment(position: Int): Fragment {
        val fragment = QuickMatchPager.newInstance(
            listOfPagerContents!!.get(position),
            coverImage,
            resultImage1,
            resultImage2,
            matchType,
            phoneNumber
        )
        return fragment
    }
}