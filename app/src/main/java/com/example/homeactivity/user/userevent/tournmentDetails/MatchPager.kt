package com.example.homeactivity.user.userevent.tournmentDetails

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.homeactivity.R
import com.example.homeactivity.user.userevent.tournmentDetails.roundDetails.RoundDetailsActivity

class MatchPager : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.match_pager, container, false)
        val btnDetails = view.findViewById<Button>(R.id.btnDetais)
        btnDetails.setOnClickListener {
            val intent = Intent(activity, RoundDetailsActivity::class.java)
            startActivity(intent)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


    }


}