package com.example.homeactivity.superadmin

import com.google.gson.annotations.SerializedName

class SuperAdminUpdateVersion {
    @SerializedName("latestVersion")
    var latestVersion: String? = null

}