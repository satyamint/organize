package com.example.homeactivity.superadmin

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.BuildConfig
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminlogin.AdminLoginActivity
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.OnItemClickListener
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import okhttp3.Headers
import retrofit2.Call
import java.util.*


private var drawerList: RecyclerView? = null
private var toolBarTitle: TextView? = null
private var relSearch: RelativeLayout? = null
private var searchLayout: SearchView? = null
private var relTool: RelativeLayout? = null
private var progressBar: ProgressBar? = null
private var btnAno: Button? = null
private var btnVer: Button? = null
private var btnLogout: Button? = null
private var tvCurrVer: TextView? = null
private var etVersion: EditText? = null
private var btnUpdate: Button? = null

private val listItems = ArrayList<Result>()
private var adapter: SuperAdminListAdapter? = null
private var cx = 0.0f
private var cy: Float = 0.0f

private var dataProccessor: SharePreferenceUtil? = null
lateinit var mAdView1: AdView

class SuperAdminListActivity : AppCompatActivity(), OnItemClickListener<Result> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_super_admin_list)
        MobileAds.initialize(this) {}
        drawerList = findViewById<View>(R.id.left_drawer) as RecyclerView
        toolBarTitle = findViewById<View>(R.id.tool_bar_title) as TextView
        tvCurrVer = findViewById<View>(R.id.tvcurrentVer) as TextView
        relSearch = findViewById<View>(R.id.relSearch) as RelativeLayout
        searchLayout = findViewById<View>(R.id.search_layout) as SearchView
        relTool = findViewById<View>(R.id.rel_tool) as RelativeLayout
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        btnVer = findViewById<View>(R.id.btnVer) as Button
        btnAno = findViewById<View>(R.id.btnAno) as Button
        btnLogout = findViewById<View>(R.id.btnLogout) as Button
        btnUpdate = findViewById<View>(R.id.btnUpdate) as Button
        etVersion = findViewById<View>(R.id.etVersion) as EditText
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        dataProccessor = SharePreferenceUtil(this@SuperAdminListActivity)
        val versionCode: Int = BuildConfig.VERSION_CODE
        val versionName: String = BuildConfig.VERSION_NAME
        tvCurrVer!!.setText("Current version Code " + versionCode + " versionName " + versionName)

        toolBarTitle!!.text = "Org List"
        listAdapter()

        btnUpdate!!.setOnClickListener() {

            if (etVersion!!.text.isEmpty()) {
                Util.toast(this@SuperAdminListActivity, "Version is empty")
                return@setOnClickListener
            } else {

                sendVersion(etVersion!!.text.toString())
            }
        }

        btnAno!!.setOnClickListener() {
            getList("ANONYMOUS")
        }
        btnVer!!.setOnClickListener() {
            getList("VERIFIED")
        }
        btnLogout!!.setOnClickListener() {
            Util.toast(this, "Logout")
            val dataProccessor = SharePreferenceUtil(this@SuperAdminListActivity)
            dataProccessor.setStr("type", "")
            dataProccessor.setStr("token", "")
            val intent = Intent(this@SuperAdminListActivity, AdminLoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        relSearch!!.visibility = View.VISIBLE
        relSearch!!.setOnClickListener(View.OnClickListener {

            keyboardUp(searchLayout!!)
        })
        searchLayout!!.setOnCloseListener {

            adapter!!.refressList(listItems)
            keyboardDown(searchLayout!!)
        }

        searchLayout!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(qString: String): Boolean {

                if (adapter != null)
                    adapter!!.filter(qString, listItems)
                return false
            }

            override fun onQueryTextSubmit(qString: String): Boolean {
                return false
            }
        })

    }

    private fun listAdapter() {
        drawerList?.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 1)
        drawerList?.layoutManager = layoutManager
        adapter = SuperAdminListAdapter(this@SuperAdminListActivity, this)
        drawerList?.adapter = adapter

        adapter!!.setList(listItems, "")

    }

    override fun onItemClick(t: Result, view: View, position: Int) {


        if ((view as Button).getTag().equals("VERIFIED")) {
            //remove the verified Admin
//            Util.toast(this@SuperAdminListActivity, "remove " + t.email)
            changeStatus(t.email.toString(), "ANONYMOUS")
        } else {
// //Add the  Admin to verification list
            changeStatus(t.email.toString(), "VERIFIED")
        }
    }

    private fun sendVersion(versionCode: String) {

        val superadmin = SuperAdminUpdateVersion()
        superadmin.latestVersion = versionCode
        progressBar!!.visibility = View.VISIBLE
        val call: Call<SuperAdminVerifyResponseModel> =
            RetrofitClient.getAPIInterface()
                .updateVersion(dataProccessor!!.getStr("token"), superadmin)
        request = RetrofitRequest(call, object : ResponseListener<SuperAdminVerifyResponseModel?> {
            override fun onResponse(response: SuperAdminVerifyResponseModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("SUPERADMIN", "response " + response!!.statusCode)

                if (response!!.status) {

                    Util.toast(this@SuperAdminListActivity, "Sucess")
                } else {
                    Util.showCustomAlert(
                        this@SuperAdminListActivity, response!!.errors.get(0).message, "Error",
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }
            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("ADMINLogin", "error " + error)
                Util.showCustomAlert(
                    this@SuperAdminListActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    private fun changeStatus(email: String, status: String) {

        val superadmin = SuperAdminUpdateStatus()
        superadmin.email = email
        superadmin.status = status

        val call: Call<SuperAdminVerifyResponseModel> =
            RetrofitClient.getAPIInterface()
                .verifyAdmin(dataProccessor!!.getStr("token"), superadmin)
        request = RetrofitRequest(call, object : ResponseListener<SuperAdminVerifyResponseModel?> {
            override fun onResponse(response: SuperAdminVerifyResponseModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("SUPERADMIN", "response " + response)

                if (response!!.status) {

                    if (status.equals("VERIFIED")) {
                        Util.toast(
                            this@SuperAdminListActivity,
                            "Successfully Added to verified  list!!"
                        )
                        getList("ANONYMOUS")
                    } else {
                        Util.toast(
                            this@SuperAdminListActivity,
                            "Successfully Removed from verified  list!!"
                        )
                        getList("VERIFIED")
                    }

                } else {
                    Util.showCustomAlert(
                        this@SuperAdminListActivity, response!!.errors.get(0).message, "Error",
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }
            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("ADMINLogin", "error " + error)
                Util.showCustomAlert(
                    this@SuperAdminListActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    var request: Request? = null
    private fun getList(type: String) {

        progressBar!!.visibility = View.VISIBLE
        val call: Call<SuperAdminModel> = RetrofitClient.getAPIInterface()
            .getSuperAdminOrgList(
                type,
                "id name status orgName profilePhoto adminGameId gameMetadata email"
            )
        request = RetrofitRequest(call, object : ResponseListener<SuperAdminModel?> {
            override fun onResponse(response: SuperAdminModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("getList", "response " + response)

                if (response!!.statusCode == 200) {

                    listItems.clear()
                    listItems.addAll(response!!.resultList!!)

                    adapter!!.setList(listItems, type)
                } else {
                    Log.d("getList", "error ")
                    Util.showCustomAlert(
                        this@SuperAdminListActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("getList", "error " + error)
                Util.showCustomAlert(
                    this@SuperAdminListActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }


    private fun keyboardDown(v2: SearchView): Boolean {
        if (cx == 0f) cx =
            relSearch!!.getX() + relSearch!!.getWidth() / 2

        if (cy == 0f) cy =
            relSearch!!.getY() + relSearch!!.getWidth() / 2

        // get the final radius for the clipping circle
//        int finalRadius = Math.max(v2.getWidth(), v2.getHeight())/2;

        // get the final radius for the clipping circle
//        int finalRadius = Math.max(v2.getWidth(), v2.getHeight())/2;
        val initialRadius = Math.hypot(cx.toDouble(), cy.toDouble()).toInt()
        // create the animator for this view (the start radius is zero)
        // create the animator for this view (the start radius is zero)
        val anim: Animator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(
                v2,
                cx.toInt(),
                cy.toInt(),
                initialRadius.toFloat(),
                0f
            )


            // make the view visible and start the animation


            // make the view invisible when the animation is done
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    v2.setVisibility(View.GONE)
                }
            })
            anim.start()
        } else {
            v2.setVisibility(View.GONE)
        }

        relSearch!!.setEnabled(true)


        return false
    }

    fun keyboardUp(v2: View) {
        relSearch?.setEnabled(true)
        v2.visibility = View.VISIBLE
        searchLayout!!.setIconified(false)
        cx = relSearch!!.getX() + relSearch!!.getWidth() / 2
        cy = relSearch!!.getY() + relSearch!!.getWidth() / 2

        // get the final radius for the clipping circle
        val endRadius = Math.hypot(cx.toDouble(), cy.toDouble()).toInt()
        // create the animator for this view (the start radius is zero)
        val anim: Animator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(
                v2,
                cx.toInt(),
                cy.toInt(),
                0f,
                endRadius.toFloat()
            )


            // make the view visible and start the animation
            anim.start()
        } else {
        }
    }


    override fun onBackPressed() {


        if (searchLayout!!.visibility == View.VISIBLE) {
            searchLayout!!.setQuery("", false)
            keyboardDown(searchLayout!!)
        } else {
            super.onBackPressed()
        }
    }
}