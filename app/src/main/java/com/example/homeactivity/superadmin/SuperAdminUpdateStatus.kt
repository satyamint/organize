package com.example.homeactivity.superadmin

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody
import java.io.File

class SuperAdminUpdateStatus {
    @SerializedName("email")
    var email: String? = null
    @SerializedName("status")
    var status: String? = null

}