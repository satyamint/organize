package com.example.homeactivity.superadmin

import com.example.homeactivity.retrofit.Error
import com.google.gson.annotations.SerializedName

class SuperAdminVerifyResponseModel {

    @SerializedName("errors")
    public lateinit var errors: List<Error>

    @SerializedName("status")
    public val status: Boolean = false

    @SerializedName("statusCode")
    public var statusCode: Int = 0
}
