package com.example.homeactivity.superadmin


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.R
import com.example.homeactivity.callbacks.OnItemClickListener
import com.squareup.picasso.Picasso
import java.util.*

class SuperAdminListAdapter(
    private val activity: SuperAdminListActivity,
    val listener: OnItemClickListener<Result>
) :
    RecyclerView.Adapter<SuperAdminListAdapter.ViewHolder>() {
    private val listItems = ArrayList<Result>()
    private val mModelList = ArrayList<Result>()
    private var adminType = ""


    class ViewHolder(itemView: View, ViewType: Int) : RecyclerView.ViewHolder(itemView) {

        internal var tvSolo: TextView = itemView.findViewById(R.id.tvSolo)
        internal var tvDuo: TextView = itemView.findViewById(R.id.tvDuo)
        internal var tvSquad: TextView = itemView.findViewById(R.id.tvSquad)
        internal var tvFree: TextView = itemView.findViewById(R.id.tvFree)
        internal var tvPaid: TextView = itemView.findViewById(R.id.tvPaid)
        internal var txtName: TextView = itemView.findViewById(R.id.text)
        internal var tvEmail: TextView = itemView.findViewById(R.id.tvEmail)
        internal var linType: LinearLayout = itemView.findViewById(R.id.linType)
        internal var tvAvilable: TextView = itemView.findViewById(R.id.tvAvilable)
        internal var tvGameId: TextView = itemView.findViewById(R.id.tvGameId)
        internal var linLangPa: LinearLayout = itemView.findViewById(R.id.linLangPa)
        internal var imgOrg: ImageView = itemView.findViewById(R.id.imgOrg)
        internal var linMatchType: LinearLayout = itemView.findViewById(R.id.linMatchType)
        internal var btnVerify: Button = itemView.findViewById(R.id.btnVerify)
        internal lateinit var parentLay: CardView
        internal lateinit var linProfile: LinearLayout


        init {
            parentLay = itemView.findViewById(R.id.gameCard)
            linProfile = itemView.findViewById(R.id.linProfile)
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, posItem: Int) {

        holder.txtName.text = listItems.get(posItem).name
        holder.tvEmail.text = listItems.get(posItem).email
        holder.tvGameId.text = listItems.get(posItem).adminGameId
        if (holder.linLangPa.childCount > 0)
            holder.linLangPa.removeAllViews()

        if (listItems.get(posItem).gameMetadata != null && listItems.get(posItem).gameMetadata!!.languages != null) {
            for (language in listItems.get(posItem).gameMetadata!!.languages!!) {
                val textLang = activity.getLayoutInflater()
                    .inflate(com.example.homeactivity.R.layout.org_tv_item, null)
                val tvLang1 = textLang.findViewById<TextView>(R.id.tvLang1)
                tvLang1.setText(language)

                holder.linLangPa.addView(textLang)
            }
        }
        holder.btnVerify.setTag(adminType)
        if (adminType.equals("VERIFIED")) {
            holder.btnVerify.setText("Remove")

        } else {
            holder.btnVerify.setText("Verify")
        }

        holder.btnVerify.setOnClickListener() {
            val position = holder.adapterPosition
            listener.onItemClick(listItems.get(position), it, position)
        }
        holder.parentLay.setBackgroundResource(com.example.homeactivity.R.drawable.cb_one)
        if (listItems.get(posItem).gameMetadata != null && listItems.get(posItem).gameMetadata!!.languages != null) {

            if (listItems.get(posItem).gameMetadata!!.totalGameEvents!! > 0) {
                holder.tvAvilable.setText("Avilable")
                holder.linType.visibility = View.VISIBLE
                holder.linMatchType.visibility = View.VISIBLE

                if (listItems.get(posItem).gameMetadata!!.hasFreeEvents!!) {
                    holder.tvFree.visibility = View.VISIBLE
                } else {
                    holder.tvFree.visibility = View.GONE
                }
                if (listItems.get(posItem).gameMetadata!!.hasPaidEvents!!) {
                    holder.tvPaid.visibility = View.VISIBLE
                } else {
                    holder.tvPaid.visibility = View.GONE
                }

                if (listItems.get(posItem).gameMetadata!!.matchTypes!!.contains("Solo")) {
                    holder.tvSolo.visibility = View.VISIBLE
                } else {
                    holder.tvSolo.visibility = View.GONE
                }
                if (listItems.get(posItem).gameMetadata!!.matchTypes!!.contains("Duo")) {
                    holder.tvDuo.visibility = View.VISIBLE
                } else {
                    holder.tvDuo.visibility = View.GONE
                }
                if (listItems.get(posItem).gameMetadata!!.matchTypes!!.contains("Squad")) {
                    holder.tvSquad.visibility = View.VISIBLE
                } else {
                    holder.tvSquad.visibility = View.GONE
                }

            } else {
                holder.linType.visibility = View.INVISIBLE
                holder.linMatchType.visibility = View.INVISIBLE
                holder.tvAvilable.setText("Not Avilable")
            }
        }else{
            holder.linType.visibility = View.INVISIBLE
            holder.linMatchType.visibility = View.INVISIBLE
            holder.tvAvilable.setText("Not Avilable")
        }


        Picasso.get()
            .load(listItems.get(posItem).profilePhoto)
            .into(holder.imgOrg);


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.super_admin_list_item, parent, false) //Inflating the layout
        return ViewHolder(v, viewType)
    }

    override fun getItemCount(): Int {
        return listItems.size

    }

    fun setList(drawerList: ArrayList<Result>, type: String) {
        adminType = type
        mModelList.clear()
        mModelList.addAll(drawerList)
        listItems.clear()
        listItems.addAll(drawerList)
        notifyDataSetChanged()
    }

    fun filter(qString: String, wholeList: ArrayList<Result>) {

        if (qString.isEmpty()) {

            if (listItems.size != wholeList.size) {

                refressList(wholeList)
            }

            return
        }
        listItems.clear()
        for (eachModel in wholeList) {
            if (eachModel.orgName!!.toLowerCase().contains(qString.toLowerCase())) {
                listItems.add(eachModel)
            } else if (eachModel.name!!.toLowerCase().contains(qString.toLowerCase())) {
                listItems.add(eachModel)
            }
        }
        notifyDataSetChanged()

    }

    fun refressList(wholeList: ArrayList<Result>) {
        mModelList.clear()
        mModelList.addAll(wholeList)
        listItems.clear()
        listItems.addAll(wholeList)
        notifyDataSetChanged()
    }
}