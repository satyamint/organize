package com.example.homeactivity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.example.homeactivity.aboutus.AboutUsActivity
import com.example.homeactivity.admin.account.adminlogin.AdminLoginActivity
import com.example.homeactivity.animateddilog.ColorDialog
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.user.org.OrgListActivity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.squareup.picasso.Picasso
import java.util.*

class MainActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null
    private var cardCreat: CardView? = null
    private var cardPlay: CardView? = null
    private var cardContactUs: CardView? = null
    private var img1: ImageView? = null
    private var img2: ImageView? = null
    private var btnCreate: Button? = null
    private var btnPlay: Button? = null
    private var btnContactUs: Button? = null
    lateinit var mAdView1: AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this) {}
        cardCreat = findViewById<CardView>(R.id.cardCreat)
        cardPlay = findViewById<CardView>(R.id.cardPlay)
        img1 = findViewById<ImageView>(R.id.img1)
        img2 = findViewById<ImageView>(R.id.img2)
        btnCreate = findViewById<Button>(R.id.btnCreate)
        btnContactUs = findViewById<Button>(R.id.btnContactUs)
        btnPlay = findViewById<Button>(R.id.btnPlay)
        cardContactUs = findViewById<CardView>(R.id.cardContactUs)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)

        cardPlay!!.setOnClickListener {
            val intent = Intent(this@MainActivity, OrgListActivity::class.java)
            startActivity(intent)
        }
        cardContactUs!!.setOnClickListener {
            val intent = Intent(this@MainActivity, AboutUsActivity::class.java)
            startActivity(intent)
        }
        btnContactUs!!.setOnClickListener {
            val intent = Intent(this@MainActivity, AboutUsActivity::class.java)
            startActivity(intent)
        }
        btnPlay!!.setOnClickListener {
            val intent = Intent(this@MainActivity, OrgListActivity::class.java)
            startActivity(intent)
        }
        cardCreat!!.setOnClickListener {
            val intent = Intent(this@MainActivity, AdminLoginActivity::class.java)
            startActivity(intent)
        }
        btnCreate!!.setOnClickListener {
            val intent = Intent(this@MainActivity, AdminLoginActivity::class.java)
            startActivity(intent)
        }

        Picasso.get()
            .load(R.drawable.gun)
            .into(img1!!)

        Picasso.get()
            .load(R.drawable.boam)
            .into(img2!!)

       /* FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            // Log and toast
            Log.d("TAG", "token -> " + token)
//            Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
        })
*/
    }

    fun showPromptDialog(view: View?) {
        showPromptDlg()
    }

    private fun showPromptDlg() {
        PromptDialog(this@MainActivity)
            .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
            .setAnimationEnable(true)
            .setTitleText("Success")
            .setContentText("Your info text goes here. Loremipsum dolor sit amet, consecteturn adipisicing elit, sed do eiusmod.")
            .setPositiveListener("OK", object : PromptDialog.OnPositiveListener {
                override fun onClick(dialog: PromptDialog) {
                    dialog.dismiss()
                }
            }).show()
    }

    fun showTextDialog(view: View?) {
        val dialog = ColorDialog(this@MainActivity)
        dialog.setColor("#8ECB54")
        dialog.setAnimationEnable(true)
        dialog.setTitle(("operation"))
        dialog.setContentText("R.string.content_text")
        dialog.setNegativeListener("Cancle", object : ColorDialog.OnNegativeListener {
            override fun onClick(dialog: ColorDialog) {
                // TODO Auto-generated method stub
                dialog.dismiss()
            }
        })
        dialog.setPositiveListener(
            ("R.string.text_iknow"),
            object : ColorDialog.OnPositiveListener {
                override fun onClick(dialog: ColorDialog) {
                    Toast.makeText(
                        this@MainActivity,
                        dialog.getPositiveText().toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }).show()
    }

    /* fun showPicDialog(v: View?) {
         val dialog = ColorDialog(this@MainActivity)
         dialog.setTitle(("R.string.operation"))
         dialog.setAnimationEnable(true)
         dialog.setAnimationIn(getInAnimationTest(this))
         dialog.setAnimationOut(getOutAnimationTest(this))
         dialog.setContentImage(resources.getDrawable(R.drawable.squad4))
         dialog.setPositiveListener(("R.string.delete"), object : ColorDialog.OnPositiveListener {
             override fun onClick(dialog: ColorDialog) {
                 Toast.makeText(
                     this@MainActivity,
                     dialog.getPositiveText().toString(),
                     Toast.LENGTH_SHORT
                 ).show()
             }
         })
             .setNegativeListener(("R.string.cancel"), object : ColorDialog.OnNegativeListener {
                 override fun onClick(dialog: ColorDialog) {
                     Toast.makeText(
                         this@MainActivity,
                         dialog.getNegativeText().toString(),
                         Toast.LENGTH_SHORT
                     ).show()
                     dialog.dismiss()
                 }
             }).show()
     }*/

    /* fun showAllModeDialog(view: View?) {
         val dialog = ColorDialog(this@MainActivity)
         dialog.setTitle(("R.string.operation"))
         dialog.setAnimationEnable(true)
         dialog.setContentText(("R.string.content_text"))
         dialog.setContentImage(resources.getDrawable(R.drawable.squad5))
         dialog.setPositiveListener(("R.string.delete"), object : ColorDialog.OnPositiveListener {
             override fun onClick(dialog: ColorDialog) {
                 Toast.makeText(
                     this@MainActivity,
                     dialog.getPositiveText().toString(),
                     Toast.LENGTH_SHORT
                 ).show()
             }
         })
         dialog.setNegativeListener(("R.string.cancel"), object : ColorDialog.OnNegativeListener {
             override fun onClick(dialog: ColorDialog) {
                 Toast.makeText(
                     this@MainActivity,
                     dialog.getNegativeText().toString(),
                     Toast.LENGTH_SHORT
                 ).show()
                 dialog.dismiss()
             }
         }).show()
     }

     fun getInAnimationTest(context: Context?): AnimationSet? {
         val out = AnimationSet(context, null)
         val alpha = AlphaAnimation(0.0f, 1.0f)
         alpha.duration = 150
         val scale = ScaleAnimation(
             0.6f,
             1.0f,
             0.6f,
             1.0f,
             Animation.RELATIVE_TO_SELF,
             0.5f,
             Animation.RELATIVE_TO_SELF,
             0.5f
         )
         scale.duration = 150
         out.addAnimation(alpha)
         out.addAnimation(scale)
         return out
     }

     fun getOutAnimationTest(context: Context?): AnimationSet? {
         val out = AnimationSet(context, null)
         val alpha = AlphaAnimation(1.0f, 0.0f)
         alpha.duration = 150
         val scale = ScaleAnimation(
             1.0f,
             0.6f,
             1.0f,
             0.6f,
             Animation.RELATIVE_TO_SELF,
             0.5f,
             Animation.RELATIVE_TO_SELF,
             0.5f
         )
         scale.duration = 150
         out.addAnimation(alpha)
         out.addAnimation(scale)
         return out
     }
 */
}