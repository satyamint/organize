package com.example.homeactivity.aboutus

import android.os.Bundle
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds

class AboutUsActivity : AppCompatActivity() {
    private var etYoutube: TextView? = null
    private var etInstagram: TextView? = null
    private var relBack: RelativeLayout? = null
    lateinit var mAdView3: AdView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        MobileAds.initialize(this) {}
        mAdView3 = findViewById(R.id.adView)
        relBack = findViewById(R.id.relBack)
        val adRequest1 = AdRequest.Builder().build()
        mAdView3.loadAd(adRequest1)

        etYoutube = findViewById<TextView>(R.id.etYoutube)
        etInstagram = findViewById<TextView>(R.id.etInstagram)

        relBack!!.setOnClickListener() {
            finish()
        }

    }
}