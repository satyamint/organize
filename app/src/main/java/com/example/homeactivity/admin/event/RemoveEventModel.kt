package com.example.homeactivity.admin.event

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

public class RemoveEventModel() : Parcelable {


    @SerializedName("statusCode")
    public var statusCode: Int = 0

    @SerializedName("status")
    public var status: Boolean = false


    constructor(parcel: Parcel) : this() {
        statusCode = parcel.readInt()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(statusCode)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventModel> {
        override fun createFromParcel(parcel: Parcel): EventModel {
            return EventModel(parcel)
        }

        override fun newArray(size: Int): Array<EventModel?> {
            return arrayOfNulls(size)
        }
    }


}

