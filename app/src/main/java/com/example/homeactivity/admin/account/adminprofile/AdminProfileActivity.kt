package com.example.homeactivity.admin.account.adminprofile

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminreg.*
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.FileUtils
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.squareup.picasso.Picasso
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call

lateinit var relEdit: RelativeLayout
lateinit var relProfileEdit: RelativeLayout
lateinit var etYoutube: EditText
lateinit var etOther: EditText
lateinit var etInstagram: EditText
lateinit var etDiscord: EditText
lateinit var linBack: LinearLayout
lateinit var img: AppCompatImageView
lateinit var tvName: EditText
lateinit var tvOrgName: TextView
lateinit var tvEmail: TextView
lateinit var tvPhone: EditText
lateinit var tvGameId: TextView
lateinit var add_photo: TextView
lateinit var btnSubmit: Button
lateinit var progressBar: ProgressBar
private var filePathimage: MultipartBody.Part? = null
private var dataProccessor: SharePreferenceUtil? = null
lateinit var addView1: AdView

class AdminProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_profile)
        relEdit = findViewById(R.id.relEdit)
        relProfileEdit = findViewById(R.id.relProfileEdit)
        etYoutube = findViewById(R.id.etYoutube)
        tvOrgName = findViewById(R.id.tvOrgName)
        etOther = findViewById(R.id.etOther)
        progressBar = findViewById(R.id.progressBar)
        etInstagram = findViewById(R.id.etInstagram)
        etDiscord = findViewById(R.id.etDiscord)
        linBack = findViewById(R.id.linBack)
        btnSubmit = findViewById(R.id.btnSubmit)
        add_photo = findViewById(R.id.add_photo)
        tvName = findViewById(R.id.tvName)
        tvEmail = findViewById(R.id.tvEmail)
        tvPhone = findViewById(R.id.tvPhone)
        tvGameId = findViewById(R.id.tvGameId)
        img = findViewById(R.id.img)
        MobileAds.initialize(this) {}
        addView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        addView1.loadAd(adRequest1)

        etYoutube.isEnabled = false
        etInstagram.isEnabled = false
        etDiscord.isEnabled = false
        etOther.isEnabled = false
        dataProccessor = SharePreferenceUtil(this@AdminProfileActivity)



        relEdit.setOnClickListener {

            Util.toast(this@AdminProfileActivity, "Now Add")
            etYoutube.isEnabled = true
            etInstagram.isEnabled = true
            etDiscord.isEnabled = true
            etOther.isEnabled = true
        }

        tvName.isEnabled = false
        tvPhone.isEnabled = false

        relProfileEdit.setOnClickListener {
            tvName.isEnabled = true
            tvPhone.isEnabled = true
        }
        btnSubmit.setOnClickListener {
            updateProfile()
        }
        img.setOnClickListener {
            openDilogForFullScreen(it)
        }

        linBack.setOnClickListener {
            intent.putExtra("needToUpdate", false)
            setResult(RESULT_OK, intent)
            finish()
        }
        getProfileDetails(dataProccessor!!.getStr("token"))
        add_photo.setOnClickListener {
            ImagePicker.with(this)
                .crop()
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
        }
        Util.hideKeyboardFrom(add_photo)
    }

    var request: Request? = null
    private fun getProfileDetails(token: String) {
        progressBar.visibility = View.VISIBLE
        val call: Call<AdminProfileModel> = RetrofitClient.getAPIInterface().getProfile(token)
        request = RetrofitRequest(call, object : ResponseListener<AdminProfileModel?> {
            override fun onResponse(response: AdminProfileModel?, headers: Headers?) {

                Log.d("getList", "response " + response)
                progressBar.visibility = View.GONE
                if (response!!.statusCode == 200) {
                    Picasso.get()
                        .load(response!!.result!!.profilePhoto)
                        .into(img)
                    tvName.setText(response.result!!.name)
                    tvEmail.setText(response.result!!.email)
                    tvPhone.setText(response.result!!.phone)

                    etYoutube.setText(response.result!!.youtube)
                    etInstagram.setText(response.result!!.instagram)
                    etDiscord.setText(response.result!!.discord)
                    etOther.setText(response.result!!.otherSocialMediaUrls)
                    tvOrgName.setText(response.result!!.orgName)
                    tvGameId.setText(response.result!!.adminGameId)
                    dataProccessor!!.setStr("profilePhoto", response.result!!.profilePhoto)
                    dataProccessor!!.setStr("orgName", response.result!!.orgName)

                } else {
                    Log.d("getList", "error ")
                    Util.showCustomAlert(
                        this@AdminProfileActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                Log.d("getList", "error " + error)
                progressBar.visibility = View.GONE
                Util.toast(this@AdminProfileActivity, "Error ")
                finish()
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar.visibility = View.GONE
                Util.toast(this@AdminProfileActivity, "Error ")
                finish()
            }

        })
        request?.enqueue()
    }

    private fun updateProfile() {
        progressBar.visibility = View.VISIBLE
        val mailId =
            RequestBody.create(MediaType.parse("multipart/form-data"), tvEmail!!.text.toString())

        val nameTxt =
            RequestBody.create(MediaType.parse("multipart/form-data"), tvName!!.text.toString())

//        val orgname =
//            RequestBody.create(MediaType.parse("multipart/form-data"), orgName!!.text.toString())

        val phNumbertxt =
            RequestBody.create(MediaType.parse("multipart/form-data"), tvPhone!!.text.toString())


        val youtube = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            etYoutube!!.text.toString()
        )
        val instagram = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            etInstagram!!.text.toString()
        )
        val discord = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            etDiscord!!.text.toString()
        )
        val otherSocialMediaUrls = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            etOther!!.text.toString()
        )
        val orgName = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            tvOrgName!!.text.toString()
        )


        val call: Call<AdminProfileModel> = RetrofitClient.getAPIInterface().updateProfile(
            dataProccessor!!.getStr("token"),
            nameTxt, mailId, phNumbertxt, instagram, discord,
            youtube, otherSocialMediaUrls, orgName, filePathimage
        )
        request = RetrofitRequest(call, object : ResponseListener<AdminProfileModel?> {
            override fun onResponse(response: AdminProfileModel?, headers: Headers?) {
                Log.d("ADMINREG", "response " + response)
                progressBar.visibility = View.GONE
                if (response!!.status) {
                    Toast.makeText(
                        this@AdminProfileActivity,
                        "Success",
                        Toast.LENGTH_SHORT
                    ).show()

                    dataProccessor!!.setStr("profilePhoto", response.result!!.profilePhoto)
                    dataProccessor!!.setStr("orgName", response.result!!.orgName)
                    setResult(RESULT_OK, intent)
                    finish()
                } else {
                    Util.showCustomAlert(
                        this@AdminProfileActivity, response.errors.get(0).message, "Error",
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                progressBar.visibility = View.GONE
                Log.d("ADMINREG", "error " + error)
                Util.showCustomAlert(
                    this@AdminProfileActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )

            }

            override fun onFailure(throwable: Throwable?) {
                progressBar.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    var alertDialog: AlertDialog? = null
    private fun openDilogForFullScreen(v: View) {


        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.fullscreen_img, null)
        dialogBuilder.setView(dialogView)

        val imageToshow = dialogView.findViewById<View>(R.id.imageToshow) as ImageView

        imageToshow.setImageDrawable((v as AppCompatImageView).drawable)
        val closeBtn = dialogView.findViewById<View>(R.id.closeBtn) as Button

        closeBtn.setOnClickListener {
            alertDialog?.dismiss()


        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }


    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!

                val fileObj = FileUtils.getFile(this@AdminProfileActivity, fileUri)

                Picasso.get()
                    .load(fileObj)
                    .into(img)

                val requestFile: RequestBody =
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        fileObj
                    )
                val image = MultipartBody.Part.createFormData(
                    "profilePhoto",
                    fileObj.getName(),
                    requestFile
                )

                filePathimage = (image)

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(RESULT_OK, intent)
        finish()
    }
}