package com.example.homeactivity.admin.event.createquickmatch

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class QuickMatchAddModel() : Parcelable {

    var isPaid: Boolean=true
    var matchType: String? = ""
    var rules: String? = ""
    var matchImage: Int = 0
    var matchList: ArrayList<MatchDetails>? = null
    var youtubeLink: String? = ""
    var gameType: String? = null
    var primaryLanguage: String? = null
    var secondaryLanguage: String? = null

    var contactDetails: ContactDetails? = null

    constructor(parcel: Parcel) : this() {
        rules = parcel.readString()
        matchType = parcel.readString()
        matchImage = parcel.readInt()
        youtubeLink = parcel.readString()
        gameType = parcel.readString()
        primaryLanguage = parcel.readString()
        secondaryLanguage = parcel.readString()

        contactDetails = parcel.readParcelable(ContactDetails::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(matchType)
        parcel.writeString(rules)
        parcel.writeInt(matchImage)
        parcel.writeString(youtubeLink)
        parcel.writeString(gameType)
        parcel.writeString(primaryLanguage)
        parcel.writeString(secondaryLanguage)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuickMatchAddModel> {
        override fun createFromParcel(parcel: Parcel): QuickMatchAddModel {
            return QuickMatchAddModel(parcel)
        }

        override fun newArray(size: Int): Array<QuickMatchAddModel?> {
            return arrayOfNulls(size)
        }
    }
}

class MatchDetails() : Parcelable {
    @SerializedName("matchSequence")
    var matchNo: String? = null
    @SerializedName("map")
    var mapName: String? = ""
    @SerializedName("startDateTime")
    var date: String? = null
    @SerializedName("isSlotAvailable")
    var slotAvilable: Boolean? = null
    @SerializedName("rules")
    var matchRulesDesc: String? = null

    constructor(parcel: Parcel) : this() {
        matchNo = parcel.readString()
        mapName = parcel.readString()
        date = parcel.readString()
        slotAvilable = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        matchRulesDesc = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(matchNo)
        parcel.writeString(mapName)
        parcel.writeString(date)
        parcel.writeValue(slotAvilable)
        parcel.writeString(matchRulesDesc)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MatchDetails> {
        override fun createFromParcel(parcel: Parcel): MatchDetails {
            return MatchDetails(parcel)
        }

        override fun newArray(size: Int): Array<MatchDetails?> {
            return arrayOfNulls(size)
        }
    }
}

class ContactDetails(): Parcelable {
    var mobileNumber: String? = ""
    var name: String? = ""
    var whatsappNumber: String? = ""

    constructor(parcel: Parcel) : this() {
        mobileNumber = parcel.readString()
        name = parcel.readString()
        whatsappNumber = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mobileNumber)
        parcel.writeString(name)
        parcel.writeString(whatsappNumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactDetails> {
        override fun createFromParcel(parcel: Parcel): ContactDetails {
            return ContactDetails(parcel)
        }

        override fun newArray(size: Int): Array<ContactDetails?> {
            return arrayOfNulls(size)
        }
    }

}

class EachQuickMatchResult() : Parcelable {
    var resultDesc: String? = null

    constructor(parcel: Parcel) : this() {
        resultDesc = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(resultDesc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EachQuickMatchResult> {
        override fun createFromParcel(parcel: Parcel): EachQuickMatchResult {
            return EachQuickMatchResult(parcel)
        }

        override fun newArray(size: Int): Array<EachQuickMatchResult?> {
            return arrayOfNulls(size)
        }
    }

}



