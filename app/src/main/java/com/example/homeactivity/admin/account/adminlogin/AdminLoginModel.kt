package com.example.homeactivity.admin.account.adminlogin

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody
import java.io.File

class AdminLoginModel {
    @SerializedName("email")
    var email: String? = null
    @SerializedName("password")
    var password: String? = null

}