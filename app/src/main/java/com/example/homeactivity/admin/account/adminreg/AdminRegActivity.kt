package com.example.homeactivity.admin.account.adminreg

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminlogin.AdminLoginActivity
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.FileUtils
import com.example.homeactivity.utility.Util
import com.example.homeactivity.utility.Util.Companion.isValidEmail
import com.example.homeactivity.utility.Util.Companion.isValidMobileNumber
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.util.*


private var img: AppCompatImageView? = null
private var person_img: ImageView? = null
private var btnSubmit: Button? = null
private var filePathimage: MultipartBody.Part? = null
private var name: EditText? = null
private var orgName: EditText? = null
private var emailId: EditText? = null
private var phNumber: EditText? = null
private var adminGaminId: EditText? = null
private var password: EditText? = null
private var rePassword: EditText? = null
private var youtube: EditText? = null
private var insta: EditText? = null
private var discord: EditText? = null
private var twitter: EditText? = null
private var progressBar: ProgressBar? = null
private var relBack: RelativeLayout? = null

class AdminRegActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_reg)

        var addPhoto = findViewById<Button>(R.id.add_photo) as TextView
        img = findViewById<AppCompatImageView>(R.id.img)
        btnSubmit = findViewById<Button>(R.id.btnSubmit)
        name = findViewById<EditText>(R.id.name)
        orgName = findViewById<EditText>(R.id.orgName)
        emailId = findViewById<EditText>(R.id.emailId)
        phNumber = findViewById<EditText>(R.id.phNumber)
        adminGaminId = findViewById<EditText>(R.id.adminGaminId)
        password = findViewById<EditText>(R.id.password)
        rePassword = findViewById<EditText>(R.id.rePassword)
        youtube = findViewById<EditText>(R.id.youtube)
        insta = findViewById<EditText>(R.id.insta)
        discord = findViewById<EditText>(R.id.discord)
        twitter = findViewById<EditText>(R.id.twitter)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)
        relBack = findViewById(R.id.relBack)

        val signInButton = findViewById<SignInButton>(R.id.sign_in_button)
        signInButton.setSize(SignInButton.SIZE_STANDARD)

        relBack!!.setOnClickListener() {
            finish()
        }
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .build()
        var mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mGoogleSignInClient.signOut()
        val signInIntent: Intent = mGoogleSignInClient.getSignInIntent()
        startForRrsultFirst.launch(signInIntent)

        signInButton.setOnClickListener {

            var mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
            mGoogleSignInClient.signOut()
            val signInIntent: Intent = mGoogleSignInClient.getSignInIntent()
            startForRrsult.launch(signInIntent)
        }
        btnSubmit!!.setOnClickListener {


            if (password!!.text.toString().isEmpty()) {

                Util.toast(this@AdminRegActivity, "Password is empty")
            } else if (rePassword!!.text.toString().isEmpty()) {

                Util.toast(this@AdminRegActivity, "Password is empty")
            } else if (!password!!.text.toString().equals("" + rePassword!!.text.toString())) {

                Util.toast(this@AdminRegActivity, "Password should be equal")
            } else if (emailId!!.text.toString().isEmpty()) {

                Util.toast(this@AdminRegActivity, "Email id is empty")
            } else if (phNumber!!.text.toString().isEmpty()) {

                Util.toast(this@AdminRegActivity, "Phone number id is empty")
            } else if (!isValidMobileNumber(phNumber!!.text.toString())) {

                Util.toast(this@AdminRegActivity, "Phone number is not valid")
            } else if (!isValidEmail(emailId!!.text.toString())) {

                Util.toast(this@AdminRegActivity, "Email id is not valid")
            } else if (adminGaminId!!.text.toString().isEmpty()) {
                Util.toast(this@AdminRegActivity, "Game ID id is empty")
            } else if (adminGaminId!!.text.toString().length < 4) {
                Util.toast(this@AdminRegActivity, "Game ID is empty")
            } else
                makeCall()
        }
        addPhoto.setOnClickListener {
            ImagePicker.with(this)
                .crop()
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
            /*
             val account = GoogleSignIn.getLastSignedInAccount(this)
             updateUI(account)*/
        }


    }

    private val startForRrsultFirst =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                emailId!!.setText("" + task.result.email)
                name!!.setText("" + task.result.displayName)
                img!!.setImageURI(task.result.photoUrl)

            }
        }
    private val startForRrsult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                emailId!!.setText("" + task.result.email)

            }
        }

    var request: Request? = null
    private fun makeCall() {

        if (filePathimage == null) {
            Util.toast(this@AdminRegActivity, "Image is compulsory")
            progressBar!!.visibility = View.GONE
            return
        }

        val mailId =
            RequestBody.create(MediaType.parse("multipart/form-data"), emailId!!.text.toString())

        val nameTxt =
            RequestBody.create(MediaType.parse("multipart/form-data"), name!!.text.toString())

        val orgname =
            RequestBody.create(MediaType.parse("multipart/form-data"), orgName!!.text.toString())

        val phNumbertxt =
            RequestBody.create(MediaType.parse("multipart/form-data"), phNumber!!.text.toString())

        val passwordtxt =
            RequestBody.create(MediaType.parse("multipart/form-data"), password!!.text.toString())

        val gameIdtxt = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            adminGaminId!!.text.toString()
        )
        val youtube = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            youtube!!.text.toString()
        )
        val instagram = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            insta!!.text.toString()
        )
        val discord = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            discord!!.text.toString()
        )
        val otherSocialMediaUrls = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            twitter!!.text.toString()
        )


        progressBar!!.visibility = View.VISIBLE

        val call: Call<AdminResponseModel> = RetrofitClient.getAPIInterface().signUpAdmin(
            nameTxt, orgname, mailId, phNumbertxt, gameIdtxt, passwordtxt, instagram, discord,
            youtube, otherSocialMediaUrls, filePathimage
        )
        request = RetrofitRequest(call, object : ResponseListener<AdminResponseModel?> {
            override fun onResponse(response: AdminResponseModel?, headers: Headers?) {
                progressBar!!.visibility = View.GONE
                Log.d("ADMINREG", "response " + response)

                if (response!!.status) {
                    var dig = PromptDialog(this@AdminRegActivity)
                        .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                        .setAnimationEnable(true)
                        .setTitleText(title)
                        .setContentText("Successfully Registered with " + response.result!!.email + "\nYour games will be vissible, once your account is verified(We need 5-7 days to verify)")
                        .setPositiveListener("OK", object : PromptDialog.OnPositiveListener {
                            override fun onClick(dialog: PromptDialog) {
                                dialog.dismiss()
                                val intent =
                                    Intent(this@AdminRegActivity, AdminLoginActivity::class.java)
                                intent.putExtra("email", response.result!!.email)
                                startActivity(intent)
                                finishAffinity()
                            }
                        })
                    dig.setCancelable(false)
                    dig.setCanceledOnTouchOutside(false)
                    dig.show()

                } else {
                    Util.showCustomAlert(
                        this@AdminRegActivity, response.errors.get(0).message, "Error",
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                Log.d("ADMINREG", "error " + error)
                progressBar!!.visibility = View.GONE
                Util.showCustomAlert(
                    this@AdminRegActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )

            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!

                img?.setImageURI(fileUri)

                val fileObj = FileUtils.getFile(this@AdminRegActivity, fileUri)
                val requestFile: RequestBody =
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        fileObj
                    )
                val image = MultipartBody.Part.createFormData(
                    "profilePhoto",
                    fileObj.getName(),
                    requestFile
                )

                filePathimage = (image)

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }


}