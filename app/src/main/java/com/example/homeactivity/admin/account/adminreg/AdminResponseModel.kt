package com.example.homeactivity.admin.account.adminreg

import com.example.homeactivity.retrofit.Error
import com.google.gson.annotations.SerializedName

class AdminResponseModel {

    @SerializedName("errors")
    public lateinit var errors: List<Error>

    @SerializedName("status")
    public val status: Boolean = false

    @SerializedName("result")
    public val result: Result? = null


}

class Result {
    @SerializedName("_id")
    public val id: String? = null

    @SerializedName("name")
    public val name: String? = null

    @SerializedName("status")
    public val status: String? = null

    @SerializedName("email")
    public val email: String? = null


}
