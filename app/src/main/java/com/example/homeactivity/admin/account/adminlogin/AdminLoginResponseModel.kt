package com.example.homeactivity.admin.account.adminlogin

import com.example.homeactivity.retrofit.Error
import com.google.gson.annotations.SerializedName

class AdminLoginResponseModel {

    @SerializedName("errors")
    public lateinit var errors: List<Error>

    @SerializedName("status")
    public val status: Boolean = false

    @SerializedName("result")
    public val result: Result? = null


}

class Result {
    @SerializedName("token")
    public val token: String? = null

    @SerializedName("userType")
    public val userType: String? = null

    @SerializedName("orgName")
    public val orgName: String? = null

    @SerializedName("profilePhoto")
    public val profilePhoto: String? = null


}