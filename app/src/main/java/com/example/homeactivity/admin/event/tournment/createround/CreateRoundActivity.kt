package com.example.viewpagerexample

import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.homeactivity.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.util.*

var adapter: CreateRoundPagerAdapter? = null

class CreateRoundActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_round)


        val pager = findViewById<ViewPager2>(R.id.pager)
        val emptyRoundtxt = findViewById<TextView>(R.id.emptyRoundtxt)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val addRoundBtn = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        emptyRoundtxt.visibility = View.VISIBLE

        adapter = CreateRoundPagerAdapter(this, this)
        pager.adapter = adapter
//        pager.orientation = ViewPager2.ORIENTATION_VERTICAL
        TabLayoutMediator(tabLayout, pager) { tab, position ->
            tab.text = "${position + 1}"


        }.attach()

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                Toast.makeText(
                    this@CreateRoundActivity,
                    "Tab ${tab?.text} selected",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                Toast.makeText(
                    this@CreateRoundActivity,
                    "Tab ${tab?.text} unselected",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                Toast.makeText(
                    this@CreateRoundActivity,
                    "Tab ${tab?.text} reselected",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
        addRoundBtn.setOnClickListener() {



            var match = Match()
            match.mapName = "Erangel"
            match.mapDateTime = "06 Oct 2021 06:30 PM"
            match.contactName = "Satyam Kumar Naik"
            match.contactNumber = "7795267445"
            val listOfMatch: ArrayList<Match> = arrayListOf()
            listOfMatch.add(match)

            var group = Group()
            group.groupName = "A"

            val groupList: ArrayList<Group> = arrayListOf()
            groupList.add(group)

            var roundModel = RoundModel()
            roundModel.roundName = "1"
            roundModel.groupList = groupList
            roundModel.matchList = listOfMatch

            (adapter)?.addRound(roundModel)

            emptyRoundtxt.visibility = View.GONE
        }

    }
}