package com.example.viewpagerexample

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.tournment.addmatch.FillMatchDetailsActivity


/**
 * A simple [Fragment] subclass.
 * Use the [RoundFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RoundFragment(round: RoundModel) : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    private var spinner: Spinner? = null
    private var addMatchBtn: Button? = null
    private var matchLinLay: LinearLayout? = null
    private var gameCard: CardView? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_round, container, false)
        spinner = view.findViewById<View>(R.id.spinner) as Spinner
        addMatchBtn = view.findViewById<View>(R.id.addMatchBtn) as Button
        matchLinLay = view.findViewById<View>(R.id.matchLinLay) as LinearLayout
        val removeTxt = view.findViewById<View>(R.id.removeTxt) as TextView
        gameCard = view.findViewById<View>(R.id.gameCard) as CardView
        removeTxt.visibility = View.INVISIBLE
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(
                    activity,
                    "Spinner " + "  ----  " + spinner!!.selectedItem.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
        gameCard?.setOnClickListener() {
            callGameDetails()
        }


        addMatchBtn?.setOnClickListener() {
            val inflater =
                activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val rowView: View = inflater.inflate(R.layout.game_match, null)

            val matchNotxt = rowView.findViewById<View>(R.id.matchNotxt) as TextView
            val removeTxt = rowView.findViewById<View>(R.id.removeTxt) as TextView
            val gameCard = rowView.findViewById<View>(R.id.gameCard) as CardView

            gameCard.setOnClickListener() {
                callGameDetails()
            }
            removeTxt.setOnClickListener() {
//                matchLinLay?.removeView(view.getParent() as View)
//                matchLinLay?.removeView(matchLinLay?.findViewById(Integer.parseInt(removeTxt.getTag() as String)))
                matchLinLay?.removeView(removeTxt.tag as View?)
                for (index in 0 until (matchLinLay as ViewGroup).childCount) {
                    val nextChild = (matchLinLay as ViewGroup).getChildAt(index)
                    val matchNotxt = nextChild.findViewById<View>(R.id.matchNotxt) as TextView
                    matchNotxt.text = "Match " + (index + 2)
                }


            }
            removeTxt.tag = (rowView)
            matchNotxt.text = "Match " + (matchLinLay?.childCount?.plus(2))
            matchLinLay?.addView(rowView)
        }



        return view
    }

    private fun callGameDetails() {
        val intent = Intent(activity, FillMatchDetailsActivity::class.java)
        startActivity(intent)

    }


}