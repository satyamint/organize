package com.example.homeactivity.admin.event.createquickmatch.edit

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminreg.AdminResponseModel
import com.example.homeactivity.admin.event.ContactDetails
import com.example.homeactivity.admin.event.MatchDetails
import com.example.homeactivity.admin.event.Result
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.FileUtils
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


private var matchLinLay: LinearLayout? = null
private var gameCard: CardView? = null
private lateinit var etMatchDetails: EditText
private lateinit var etYotubeLink: EditText
private lateinit var etResultDesc: EditText
private lateinit var etPhNo: EditText
private lateinit var matchNotxt: TextView
private lateinit var img1: ImageView
private lateinit var imgResult1: ImageView
private lateinit var imgResult2: ImageView
private lateinit var btnSubmit: Button
private lateinit var tvDate: TextView
private lateinit var tvHour: TextView
private lateinit var tvMapName: TextView
private lateinit var linBack: LinearLayout
private var finalResultobj: Result? = null
private var relRemovePhoto: RelativeLayout? = null
private var relResultRemovePhoto1: RelativeLayout? = null
private var relResultRemovePhoto2: RelativeLayout? = null
private var multiPartImageList: MutableList<MultipartBody.Part> =
    mutableListOf<MultipartBody.Part>()
private var progressBar: ProgressBar? = null
private var matchTypeSpinner: Spinner? = null
var hashMap: HashMap<String, MultipartBody.Part> = HashMap<String, MultipartBody.Part>()
private var rbFree: RadioButton? = null
private var rbPaid: RadioButton? = null
lateinit var radioGP: RadioGroup
lateinit var relResultPhoto1: RelativeLayout
lateinit var relAddPhoto: RelativeLayout
lateinit var relResultPhoto2: RelativeLayout
lateinit var primaryLangSpinner: Spinner
lateinit var secondaryLangSpinner: Spinner

private var switch1: Switch? = null
var request: Request? = null
var filesToRemove: HashMap<String, String> = HashMap<String, String>()
var arrarListToremove: ArrayList<String> = ArrayList<String>()
lateinit var mAdView1: AdView

class QuickMatchEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_match_edit)
        MobileAds.initialize(this) {}
        matchLinLay = findViewById<View>(R.id.matchLinLay) as LinearLayout
        val removeTxt = findViewById<View>(R.id.removeTxt) as TextView
        gameCard = findViewById<View>(R.id.gameCard) as CardView
        etMatchDetails = findViewById(R.id.etMatchDetails)
        etYotubeLink = findViewById(R.id.etYotubeLink)
        relAddPhoto = findViewById(R.id.relAddPhoto)
        etPhNo = findViewById(R.id.etPhNo)
        rbFree = findViewById(R.id.rbFree)
        progressBar = findViewById(R.id.progressBar)
        radioGP = findViewById(R.id.radioGP)
        rbPaid = findViewById(R.id.rbPaid)
        tvDate = findViewById(R.id.tvDate)
        relResultPhoto1 = findViewById(R.id.relResultPhoto1)
        relResultPhoto2 = findViewById(R.id.relResultPhoto2)
        tvHour = findViewById<View>(R.id.tvHour) as TextView
        img1 = findViewById(R.id.img1)
        imgResult1 = findViewById(R.id.imgResultone)
        relRemovePhoto = findViewById(R.id.relRemovePhoto)
        primaryLangSpinner = findViewById(R.id.spinnerLang1)
        secondaryLangSpinner = findViewById(R.id.spinnerLang2)
        relResultRemovePhoto1 = findViewById(R.id.relResultRemovePhoto1)
        relResultRemovePhoto2 = findViewById(R.id.relResultRemovePhoto2)
        imgResult2 = findViewById(R.id.imgResulttwo)
        etResultDesc = findViewById(R.id.etResultDesc)
        linBack = findViewById(R.id.linBack)
        matchNotxt = findViewById<View>(R.id.matchNotxt) as TextView
        removeTxt.visibility = View.INVISIBLE
        matchTypeSpinner = findViewById<Spinner>(R.id.gameTypeSpinner)
        btnSubmit = findViewById(R.id.btnSubmit)
        tvMapName = findViewById(R.id.tvMapName)

        switch1 = findViewById(R.id.switch1)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)

        switch1!!.setOnCheckedChangeListener { _, isChecked ->

        }
        progressBar!!.visibility = View.GONE

        finalResultobj = intent.getParcelableExtra<Result>("Resultobj")
        finalResultobj!!.contactDetails =
            intent.getParcelableExtra<ContactDetails>("ContactDetails")
        finalResultobj!!.matchList = intent.getParcelableArrayListExtra("listResult")

        switch1!!.isChecked = finalResultobj!!.isSlotAvailable!!
        filesToRemove.put("coverImage", "")
        filesToRemove.put("resultImage1", "")
        filesToRemove.put("resultImage2", "")
        hashMap.clear()
        clickListeners()
        setValues()
        fillNoOfMatch(finalResultobj!!)
        Util.hideKeyboardFrom(etMatchDetails)


    }

    private fun setValues() {
        etMatchDetails.setText(finalResultobj!!.rules)
        if (finalResultobj!!.youtubeLink != null)
            etYotubeLink.setText(finalResultobj!!.youtubeLink)
        etPhNo.setText(finalResultobj!!.contactDetails!!.mobileNumber)
        if (finalResultobj!!.result != null)
            etResultDesc.setText(finalResultobj!!.result)
        Picasso.get()
            .load(finalResultobj!!.coverImage)
            .into(img1)
        Picasso.get()
            .load(finalResultobj!!.resultImage1)
            .into(imgResult1)
        Picasso.get()
            .load(finalResultobj!!.resultImage2)
            .into(imgResult2)

        if (finalResultobj!!.isPaid!!) {
            rbPaid!!.isChecked = true
            rbFree!!.isChecked = false
        } else {
            rbPaid!!.isChecked = false
            rbFree!!.isChecked = true
        }

        primaryLangSpinner.setSelection(getLangSelectoion(finalResultobj!!.primaryLanguage))
        secondaryLangSpinner.setSelection(getLangSelectoion(finalResultobj!!.secondaryLanguage))
        matchTypeSpinner!!.setSelection(getMatchTypeSelection(finalResultobj!!.matchType))


    }


    private fun clickListeners() {


        primaryLangSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                finalResultobj!!.primaryLanguage = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        secondaryLangSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                finalResultobj!!.secondaryLanguage = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })


        matchTypeSpinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                finalResultobj!!.matchType = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        relRemovePhoto!!.setOnClickListener {

            img1.setImageDrawable(null)
            if (hashMap.get("coverImage") != null) {
                hashMap.remove("coverImage")
            }

            filesToRemove.put("coverImage", "" + finalResultobj!!.coverImagePath!!)
        }
        relResultRemovePhoto1!!.setOnClickListener {

            imgResult1.setImageDrawable(null)
            if (hashMap.get("resultImage1") != null) {
                hashMap.remove("resultImage1")
            }

            if (finalResultobj!!.resultImagePath1 != null)
                filesToRemove.put("resultImage1", "" + finalResultobj!!.resultImagePath1!!)

        }
        relResultRemovePhoto2!!.setOnClickListener {

            imgResult2.setImageDrawable(null)
            if (hashMap.get("resultImage2") != null) {
                hashMap.remove("resultImage2")
            }

            if (finalResultobj!!.resultImagePath2 != null)
                filesToRemove.put("resultImage2", "" + finalResultobj!!.resultImagePath2!!)

        }

        etMatchDetails.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                finalResultobj?.rules = s.toString()
            }

        })
        etResultDesc.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {

                finalResultobj!!.result = s.toString()
            }

        })
        etPhNo.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                finalResultobj!!.contactDetails!!.mobileNumber = s.toString()
            }

        })
        etYotubeLink.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                finalResultobj?.youtubeLink = s.toString()
            }

        })
        linBack!!.setOnClickListener {
            intent.putExtra("needToUpdate", false)
            setResult(RESULT_OK, intent)
            finish()
        }
        btnSubmit!!.setOnClickListener {
            val gson = Gson()
            val json = gson.toJson(finalResultobj)
            Log.d("TAG", "finalResultobj---> " + json)


            submitCall()
        }

        gameCard?.setOnClickListener() {
            callGameDetails(matchNotxt)
        }


        img1?.setOnClickListener() {
            openDilogForFullScreen(it)
        }
        imgResult1?.setOnClickListener() {
            openDilogForFullScreen(it)
        }
        imgResult2?.setOnClickListener() {
            openDilogForFullScreen(it)
        }
        relAddPhoto?.setOnClickListener() {
            ImagePicker.with(this)
                .crop()
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }

//            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
//            startForProfileImageResult.launch(gallery)
        }

        relResultPhoto1?.setOnClickListener() {
            ImagePicker.with(this)
                .crop()
                .createIntent { intent ->
                    startForResultImage1.launch(intent)
                }
        }
        relResultPhoto2?.setOnClickListener() {
            ImagePicker.with(this)
                .crop()
                .createIntent { intent ->
                    startForResultImage2.launch(intent)
                }
        }
    }


    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!

                val fileObj = FileUtils.getFile(this@QuickMatchEditActivity, fileUri)
                Picasso.get()
                    .load(fileObj)
                    .into(img1)

                val requestFile: RequestBody =
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        fileObj
                    )
                val image = MultipartBody.Part.createFormData(
                    "coverImage",
                    fileObj.getName(),
                    requestFile
                )
                filesToRemove.put("coverImage", "")
                hashMap.put("coverImage", image)

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    private val startForResultImage1 =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!
                val fileObj1 = FileUtils.getFile(this@QuickMatchEditActivity, fileUri)

                Picasso.get()
                    .load(fileObj1)
                    .into(imgResult1)
                Log.d("TAG", "imgResult111111 " + fileUri)
                Log.d("TAG", "imgResult111111 " + fileObj1.path)

                val requestFile: RequestBody =
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        fileObj1
                    )
                val image = MultipartBody.Part.createFormData(
                    "resultImage1",
                    fileObj1.getName(),
                    requestFile
                )
                filesToRemove.put("resultImage1", "")
                hashMap.put("resultImage1", image)

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    private val startForResultImage2 =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!
                val fileObj2 = FileUtils.getFile(this@QuickMatchEditActivity, fileUri)
                Log.d("TAG", "imgResult2 " + fileUri)
                Picasso.get()
                    .load(fileObj2)
                    .into(imgResult2)
                val requestFile: RequestBody =
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        fileObj2
                    )
                val image = MultipartBody.Part.createFormData(
                    "resultImage2",
                    fileObj2.getName(),
                    requestFile
                )
                filesToRemove.put("resultImage2", "")
                hashMap.put("resultImage2", image)
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }


    private fun callGameDetails(matchNotxt: TextView) {

        val intent = Intent(this, FillQuickmatchEditDetails::class.java)
        intent.putExtra("matchNo", matchNotxt.text.toString())
        intent.putExtra("matchType", finalResultobj!!.matchType)
        intent.putExtra("contactDetails", finalResultobj!!.contactDetails)
        if (finalResultobj!!.matchList != null && finalResultobj!!.matchList!!.size >= (Integer.parseInt(
                matchNotxt.text.toString()
            ))
        )
            intent.putExtra(
                "Matchobject",
                finalResultobj!!.matchList?.get(Integer.parseInt(matchNotxt.text.toString()) - 1)
            )
        startForActivityResult.launch(intent)

    }

    private val startForActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK && data != null) {
                val matchDetails = data.getParcelableExtra<MatchDetails>("MatchDetails")

                if (matchDetails != null) {
                    Toast.makeText(
                        this,
                        " " + data.getStringExtra("id") + " ${matchDetails.matchSequence}",
                        Toast.LENGTH_SHORT
                    ).show()
                    if (finalResultobj!!.matchList == null)
                        finalResultobj!!.matchList = arrayListOf<MatchDetails>()

                    finalResultobj!!.matchList!!.set(
                        (Integer.parseInt(matchDetails.matchSequence) - 1),
                        matchDetails
                    )

                    fillNoOfMatch(finalResultobj!!)


                } else {
                    Toast.makeText(
                        this,
                        " null return",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    private fun fillNoOfMatch(quickMatchObj: Result) {
        matchLinLay!!.removeAllViews()
        for (index in 0 until quickMatchObj!!.matchList!!.size) {

            if (index == 0) {
                matchNotxt.text = "" + quickMatchObj!!.matchList!!.get(0).matchSequence
                var dtTm = quickMatchObj!!.matchList!!.get(0).startDateTime
                tvDate.text = "" + dtTm!!.split("-")[0]
                dtTm = quickMatchObj!!.matchList!!.get(0).startDateTime
                tvHour.text = "" + dtTm!!.split("-")[1]
                tvMapName.text = "" + quickMatchObj!!.matchList!!.get(0).mapName
            } else {

                val inflater =
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val rowView: View = inflater.inflate(R.layout.quickmatch_add, null)

                val matchNotxt = rowView.findViewById<View>(R.id.matchNotxt) as TextView
                val removeTxt = rowView.findViewById<View>(R.id.removeTxt) as TextView
                val gameCard = rowView.findViewById<View>(R.id.gameCard) as CardView
                val tvDate = rowView.findViewById<View>(R.id.tvDate) as TextView
                val tvHour = rowView.findViewById<View>(R.id.tvHour) as TextView
                val tvMapName = rowView.findViewById<View>(R.id.tvMapName) as TextView

                gameCard.setOnClickListener() {
                    callGameDetails(matchNotxt)
                }

                removeTxt.tag = (rowView)
                matchNotxt.text = "" + (matchLinLay?.childCount?.plus(2))

                tvDate!!.setText(quickMatchObj!!.matchList!!.get(index).startDateTime!!.split("-")[0])
                tvHour!!.setText(quickMatchObj!!.matchList!!.get(index).startDateTime!!.split("-")[1])
                tvMapName!!.setText(quickMatchObj!!.matchList!!.get(index).mapName)
                matchLinLay?.addView(rowView)

                removeTxt.visibility = View.INVISIBLE
            }

        }
    }

    private fun submitCall() {
        val genid: Int = radioGP.getCheckedRadioButtonId()
        val radioButton = findViewById<View>(genid) as RadioButton
        val typee = radioButton.text.toString()

        finalResultobj!!.isPaid = typee.equals("Paid")


        var wholeListMap = HashMap<String, RequestBody>()

        val gsonObj = Gson()
        val contactDetails = gsonObj.toJson(finalResultobj!!.contactDetails)

        Log.d(
            "TAG",
            "---> contactDetails " + contactDetails
        )

        wholeListMap.put(
            "name",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "Quick Match "
            )
        )

        wholeListMap.put(
            "_id",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + finalResultobj!!._id
            )
        )


        wholeListMap.put(
            "quickMatchResultDesc",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + finalResultobj!!.result
            )
        )
        wholeListMap.put(
            "contactDetails",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + contactDetails
            )
        )
        val gson = Gson()
        val json = gson.toJson(finalResultobj!!.matchList)
        Log.d("TAG", "matchList---> " + json)

        arrarListToremove.clear()

        if (!filesToRemove.get("coverImage")!!.isEmpty()) {
            arrarListToremove.add('"' + filesToRemove.get("coverImage")!! + '"')

            wholeListMap.put(
                "coverImage",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )

            wholeListMap.put(
                "coverImagePath",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )
        }
        if (!filesToRemove.get("resultImage1")!!.isEmpty()) {
            arrarListToremove.add('"' + filesToRemove.get("resultImage1")!! + '"')
            wholeListMap.put(
                "resultImage1",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )

            wholeListMap.put(
                "resultImagePath1",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )
        }
        if (!filesToRemove.get("resultImage2")!!.isEmpty()) {
            arrarListToremove.add('"' + filesToRemove.get("resultImage2")!! + '"')

            wholeListMap.put(
                "resultImage2",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )

            wholeListMap.put(
                "resultImagePath2",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )
        }

        Log.d(
            "TAG",
            "filesToRemove---> " + (arrarListToremove.toString() + " --- ")
        )
        wholeListMap.put(
            "filesToRemove",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                arrarListToremove.toString()
            )
        )

        wholeListMap.put(
            "matchList",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                json
            )
        )


        wholeListMap.put(
            "isPaid",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + finalResultobj!!.isPaid
            )
        )


        wholeListMap.put(
            "isSlotAvailable",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + switch1!!.isChecked
            )
        )
        wholeListMap.put(
            "matchType",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                finalResultobj!!.matchType
            )
        )
        wholeListMap.put(
            "rules",
            RequestBody.create(MediaType.parse("multipart/form-data"), finalResultobj!!.rules)
        )

        wholeListMap.put(
            "youtubeLink",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + finalResultobj!!.youtubeLink
            )
        )
        wholeListMap.put(
            "gameType",
            RequestBody.create(MediaType.parse("multipart/form-data"), finalResultobj!!.gameType)
        )
        wholeListMap.put(
            "primaryLanguage",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + finalResultobj!!.primaryLanguage
            )
        )
        wholeListMap.put(
            "secondaryLanguage",
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                "" + finalResultobj!!.secondaryLanguage
            )
        )

        if (finalResultobj!!.matchList != null && finalResultobj!!.matchList!!.size > 0)
            wholeListMap.put(
                "startDateTime",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    finalResultobj!!.matchList!!.get(0).startDateTime
                )
            )
        if (finalResultobj!!.matchList != null && finalResultobj!!.matchList!!.size > 0)
            wholeListMap.put(
                "createdDateTime",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    finalResultobj!!.matchList!!.get(0).startDateTime
                )
            )
        multiPartImageList.clear()
        if (hashMap.get("coverImage") != null)
            multiPartImageList.add(hashMap.get("coverImage")!!)

        if (hashMap.get("resultImage1") != null)
            multiPartImageList.add(hashMap.get("resultImage1")!!)



        if (hashMap.get("resultImage2") != null)
            multiPartImageList.add(hashMap.get("resultImage2")!!)

        val dataProccessor = SharePreferenceUtil(this@QuickMatchEditActivity)

        progressBar!!.visibility = View.VISIBLE
        val call: Call<AdminResponseModel> = RetrofitClient.getAPIInterface()
            .updateMatch(
                dataProccessor.getStr("token"),
                wholeListMap,
                multiPartImageList
            )
        request = RetrofitRequest(call, object : ResponseListener<AdminResponseModel?> {
            override fun onResponse(response: AdminResponseModel?, headers: Headers?) {
                progressBar!!.visibility = View.GONE
                Log.d("TAG", "response " + response)


                if (response!!.status) {
                    intent.putExtra("needToUpdate", true)
                    setResult(RESULT_OK, intent)
                    finish()
                }


            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("TAG", "error " + error)
                Toast.makeText(
                    this@QuickMatchEditActivity,
                    "error",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    private fun getMatchTypeSelection(language: String?): Int {

        when (language!!.toLowerCase()) {

            "solo" -> {

                return 0
            }
            "duo" -> {
                return 1
            }
            else -> {
                return 2
            }
        }


    }

    private fun getLangSelectoion(language: String?): Int {

        Log.d("TAG", "getLangSelectoion " + language)
        when (language!!.toLowerCase()) {

            "hindi" -> {

                return 1
            }
            "kannada" -> {
                return 2
            }
            "tamil" -> {
                return 3
            }
            "telugu" -> {
                return 4
            }
            "odia" -> {
                return 5
            }
            "bengali" -> {
                return 6
            }
            "punjabi" -> {
                return 7
            }
            "malayalam" -> {
                return 8
            }
            "assamese" -> {
                return 9
            }
            "gujarati" -> {
                return 10
            }
            "marathi" -> {
                return 11
            }
            else -> {
                return 0
            }
        }


    }

    var alertDialog: AlertDialog? = null
    private fun openDilogForFullScreen(v: View) {


        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.fullscreen_img, null)
        dialogBuilder.setView(dialogView)

        val imageToshow = dialogView.findViewById<View>(R.id.imageToshow) as ImageView

        imageToshow.setImageDrawable((v as AppCompatImageView).drawable)
        val closeBtn = dialogView.findViewById<View>(R.id.closeBtn) as Button

        closeBtn.setOnClickListener {
            alertDialog?.dismiss()


        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }


}