package com.example.homeactivity.admin.event

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

public class EventModel() : Parcelable {


    @SerializedName("statusCode")
    public var statusCode: Int = 0

    @SerializedName("status")
    public var status: Boolean = false

    @SerializedName("result")
    public var resultList: ArrayList<Result>? = null

    constructor(parcel: Parcel) : this() {
        statusCode = parcel.readInt()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(statusCode)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventModel> {
        override fun createFromParcel(parcel: Parcel): EventModel {
            return EventModel(parcel)
        }

        override fun newArray(size: Int): Array<EventModel?> {
            return arrayOfNulls(size)
        }
    }


}

public class Result() : Parcelable {
    @SerializedName("_id")
    public var _id: String? = null

    @SerializedName("userId")
    public var userId: String? = null

    @SerializedName("name")
    public var name: String? = null

    @SerializedName("matchType")
    public var matchType: String? = null

    @SerializedName("gameType")
    public var gameType: String? = null

    @SerializedName("coverImage")
    public var coverImage: String? = null

    @SerializedName("coverImagePath")
    public var coverImagePath: String? = null

    @SerializedName("isPaid")
    public var isPaid: Boolean? = false

    @SerializedName("isSlotAvailable")
    public var isSlotAvailable: Boolean? = false

    @SerializedName("startDateTime")
    public var startDateTime: String? = null

    @SerializedName("rules")
    public var rules: String? = null

    @SerializedName("primaryLanguage")
    public var primaryLanguage: String? = ""

    @SerializedName("secondaryLanguage")
    public var secondaryLanguage: String? = ""

    @SerializedName("matchList")
    var matchList: ArrayList<MatchDetails>? = null

    @SerializedName("contactDetails")
    var contactDetails: ContactDetails? = null

    @SerializedName("quickMatchResultDesc")
    var result: String? = ""

    @SerializedName("resultImage1")
    var resultImage1: String? = null

    @SerializedName("resultImagePath1")
    var resultImagePath1: String? = null

    @SerializedName("resultImage2")
    var resultImage2: String? = null

    @SerializedName("resultImagePath2")
    var resultImagePath2: String? = null

    @SerializedName("youtubeLink")
    var youtubeLink: String? = null

    @SerializedName("filesToRemove")
    var filesToRemove: ArrayList<String>? = null

    constructor(parcel: Parcel) : this() {
        _id = parcel.readString()
        userId = parcel.readString()
        name = parcel.readString()
        matchType = parcel.readString()
        gameType = parcel.readString()
        coverImage = parcel.readString()
        coverImagePath = parcel.readString()
        isPaid = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        isSlotAvailable = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        startDateTime = parcel.readString()
        rules = parcel.readString()
        primaryLanguage = parcel.readString()
        secondaryLanguage = parcel.readString()
        contactDetails = parcel.readParcelable(ContactDetails::class.java.classLoader)
        result = parcel.readString()
        resultImage1 = parcel.readString()
        resultImagePath1 = parcel.readString()
        resultImage2 = parcel.readString()
        resultImagePath2 = parcel.readString()
        youtubeLink = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(userId)
        parcel.writeString(name)
        parcel.writeString(matchType)
        parcel.writeString(gameType)
        parcel.writeString(coverImage)
        parcel.writeString(coverImagePath)
        parcel.writeValue(isPaid)
        parcel.writeValue(isSlotAvailable)
        parcel.writeString(startDateTime)
        parcel.writeString(rules)
        parcel.writeString(primaryLanguage)
        parcel.writeString(secondaryLanguage)
        parcel.writeParcelable(contactDetails, flags)
        parcel.writeString(result)
        parcel.writeString(resultImage1)
        parcel.writeString(resultImagePath1)
        parcel.writeString(resultImage2)
        parcel.writeString(resultImagePath2)
        parcel.writeString(youtubeLink)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Result> {
        override fun createFromParcel(parcel: Parcel): Result {
            return Result(parcel)
        }

        override fun newArray(size: Int): Array<Result?> {
            return arrayOfNulls(size)
        }
    }


}

class MatchDetails() : Parcelable {
    @SerializedName("matchSequence")
    var matchSequence: String? = null

    @SerializedName("map")
    var mapName: String? = null

    @SerializedName("startDateTime")
    var startDateTime: String? = ""


    @SerializedName("rules")
    var matchRulesDesc: String? = null

//    var contactNo: String? = null

    constructor(parcel: Parcel) : this() {
        matchSequence = parcel.readString()
        mapName = parcel.readString()
        startDateTime = parcel.readString()
        matchRulesDesc = parcel.readString()
//        contactNo = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(matchSequence)
        parcel.writeString(mapName)
        parcel.writeString(startDateTime)
        parcel.writeString(matchRulesDesc)
//        parcel.writeString(contactNo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MatchDetails> {
        override fun createFromParcel(parcel: Parcel): MatchDetails {
            return MatchDetails(parcel)
        }

        override fun newArray(size: Int): Array<MatchDetails?> {
            return arrayOfNulls(size)
        }
    }


}

class ContactDetails() : Parcelable {
    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("name")
    var name: String? = null

    constructor(parcel: Parcel) : this() {
        email = parcel.readString()
        mobileNumber = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(mobileNumber)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactDetails> {
        override fun createFromParcel(parcel: Parcel): ContactDetails {
            return ContactDetails(parcel)
        }

        override fun newArray(size: Int): Array<ContactDetails?> {
            return arrayOfNulls(size)
        }
    }


}


