package com.example.homeactivity.admin.event.createquickmatch.edit

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.ContactDetails
import com.example.homeactivity.admin.event.MatchDetails
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*


private var tvM: TextView? = null
private var tvDate: TextView? = null
private var matchNo: TextView? = null
private var year: Int? = null
private var month: Int? = null
private var day: Int? = null
private var hour: Int? = null
private var minute: Int? = null
private var btnSave: Button? = null
private var matchDetails: MatchDetails? = null
private var tvMatchType: TextView? = null
private var mapSpinner: Spinner? = null
private var etGameDesc: EditText? = null
private var etContactNo: TextView? = null
private var linDateTime: LinearLayout? = null
private var linBack: LinearLayout? = null
lateinit var mAdView2: AdView

class FillQuickmatchEditDetails : AppCompatActivity(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MobileAds.initialize(this) {}
        setContentView(R.layout.activity_fill_quickmatch_edit_details)
        tvMatchType = findViewById<TextView>(R.id.tvMatchType)
        mapSpinner = findViewById<Spinner>(R.id.mapSpinner)
        tvDate = findViewById<TextView>(R.id.tvDate)
        tvM = findViewById<TextView>(R.id.tvM)
        matchNo = findViewById<TextView>(R.id.matchNo)
        btnSave = findViewById<Button>(R.id.btnSave)
        etGameDesc = findViewById<EditText>(R.id.etGameDesc)
        etContactNo = findViewById<TextView>(R.id.etContactNo)
        linDateTime = findViewById(R.id.linDateTime)
        linBack = findViewById(R.id.linBack)
        mAdView2 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView2.loadAd(adRequest1)

        val matchNoStr: String = intent.getStringExtra("matchNo").toString()
        val matchType: String = intent.getStringExtra("matchType").toString()

        tvMatchType!!.setText(matchType)
        matchDetails = intent.getParcelableExtra<MatchDetails>("Matchobject")
        val contactDetails = intent.getParcelableExtra<ContactDetails>("contactDetails")
        if (contactDetails != null) {
            etContactNo!!.setText(contactDetails!!.mobileNumber)
        }

        if (matchDetails != null) {
            etGameDesc!!.setText(matchDetails!!.matchRulesDesc)
            tvM!!.setText(matchDetails!!.startDateTime!!.split("-")[0])
            tvDate!!.setText("" + matchDetails!!.startDateTime!!.split("-")[1])
            if (matchDetails!!.mapName.equals("Miramar")) {
                mapSpinner!!.setSelection(1)
            } else if (matchDetails!!.mapName.equals("Sanhok")) {
                mapSpinner!!.setSelection(2)
            } else if (matchDetails!!.mapName.equals("Vikendi")) {
                mapSpinner!!.setSelection(3)
            } else {
                mapSpinner!!.setSelection(0)
            }


//            if (matchDetails!!.result != null) {
//                if (matchDetails!!!!.result!!.resultDesc != null) {
//                    etResultDesc!!.setText(matchDetails!!.result!!.resultDesc)
//                }
//            }
        }

        matchNo!!.setText(matchNoStr)
//        matchDetails!!.matchSequence = matchNoStr

        clickListeners()


    }

    private fun clickListeners() {

        val map = mapSpinner!!.getSelectedItem().toString()
        matchDetails!!.mapName = map


        etGameDesc!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null)
                    matchDetails!!.matchRulesDesc = s.toString()
                else
                    matchDetails!!.matchRulesDesc = ""
            }
        })
        mapSpinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                matchDetails!!.mapName = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })


        /*swResult!!.setOnCheckedChangeListener { buttonView, isChecked ->

            matchDetails!!.slotAvilable = isChecked
            if (isChecked) {
                linResult!!.visibility = View.VISIBLE
            } else {
                linResult!!.visibility = View.GONE
            }

        }*/

        btnSave!!.setOnClickListener {


            val gson = Gson()
            val json = gson.toJson(matchDetails)
            Log.d("TAG", "---> " + json)


            if (matchDetails!!.matchRulesDesc!!.isEmpty()) {

                etGameDesc!!.setError("Cannot be Empty")
            } else if (matchDetails!!.startDateTime!!.isEmpty()) {

                Toast.makeText(this, "Please select any date ", Toast.LENGTH_SHORT).show()
            } /*else if (matchDetails!!.time!!.isEmpty()) {
                Toast.makeText(this, "Please select time ", Toast.LENGTH_SHORT).show()
            } */ /*else if (matchDetails!!.contactNo!!.isEmpty()) {
                etContactNo!!.setError("Cannot be Empty")
            } */ else {
                val intent = Intent()
                intent.putExtra("id", "return value")
                intent.putExtra("MatchDetails", matchDetails)
                setResult(RESULT_OK, intent)
                finish()
            }


        }


        linDateTime!!.setOnClickListener {
            val calendar: Calendar = Calendar.getInstance()
            year = calendar.get(Calendar.YEAR)
            month = calendar.get(Calendar.MONTH)
            day = calendar.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog =
                DatePickerDialog(
                    this@FillQuickmatchEditDetails, this@FillQuickmatchEditDetails,
                    year!!, month!!, day!!
                )
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + (24 * 3600000))
            datePickerDialog.show()
        }

        linBack!!.setOnClickListener {
            finish()
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {

        val cal = Calendar.getInstance()
        val month_date = SimpleDateFormat("MMM")
        val monthnum = p2
        cal[Calendar.MONTH] = monthnum


        matchDetails!!.startDateTime = "" + p3 + "" + month_date.format(cal.time) + p1
        tvDate!!.setText("" + matchDetails!!.startDateTime)
        val c = Calendar.getInstance()
        hour = c[Calendar.HOUR]
        minute = c[Calendar.MINUTE]
        val timePickerDialog = TimePickerDialog(
            this@FillQuickmatchEditDetails,
            this@FillQuickmatchEditDetails,
            hour!!,
            minute!!,
            DateFormat.is24HourFormat(this)
        )
        timePickerDialog.show()

    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {

        val s = "$p1:$p2"
        val f1 = SimpleDateFormat("HH:mm") //HH for hour of the day (0 - 23)

        val d: Date = f1.parse(s)
        val hr = SimpleDateFormat("h")
        val min = SimpleDateFormat("mm")
        val amPm = SimpleDateFormat("aa")


        val time =
            hr.format(d).toLowerCase() + ":" + min.format(d).toLowerCase() + ":" + amPm.format(d)
                .toLowerCase()
        matchDetails!!.startDateTime =
            matchDetails!!.startDateTime + "-" + time

        tvM!!.setText(time)
    }


}