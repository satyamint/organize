package com.example.homeactivity.admin.account.adminlogin

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminreg.*
import com.example.homeactivity.admin.event.CreatEventActivity
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.superadmin.SuperAdminListActivity
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import okhttp3.Headers
import retrofit2.Call

class AdminLoginActivity : AppCompatActivity() {

    private var btnLogin: Button? = null
    private var btnRegister: Button? = null
    private var mailid: EditText? = null
    private var password: EditText? = null
    private var progressBar: ProgressBar? = null
    lateinit var mAdView1: AdView
    private var relBack: RelativeLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_login)
        MobileAds.initialize(this) {}
        btnLogin = findViewById<Button>(R.id.btnLogin)
        btnRegister = findViewById<Button>(R.id.btnRegister)
        mailid = findViewById<EditText>(R.id.maidId)
        relBack = findViewById(R.id.relBack)
        password = findViewById<EditText>(R.id.password)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)
        progressBar!!.visibility = View.GONE
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        val mail = intent.getStringExtra("email")

        if (mail != null) {
            mailid!!.setText(mail)
        }
        btnRegister!!.setOnClickListener {
            if (isPermissionsAllowed()) {
                val intent = Intent(this@AdminLoginActivity, AdminRegActivity::class.java)
                startActivity(intent)
            } else {
                askForPermissions()
            }
        }
        relBack!!.setOnClickListener() {
            finish()
        }

        btnLogin!!.setOnClickListener {
            if (isPermissionsAllowed()) {
                progressBar!!.visibility = View.VISIBLE
                makeCall(mailid!!.text.toString(), password!!.text.toString())
            } else {
                askForPermissions()
            }
        }

        val dataProccessor = SharePreferenceUtil(this@AdminLoginActivity)
        if (!dataProccessor.getStr("type").isEmpty()) {
            if (dataProccessor.getStr("type").equals("admin")) {
                val intent = Intent(this@AdminLoginActivity, CreatEventActivity::class.java)
                startActivity(intent)
                finish()
            } else if (dataProccessor.getStr("type").equals("superAdmin")) {
                val intent = Intent(this@AdminLoginActivity, SuperAdminListActivity::class.java)
                startActivity(intent)
                finish()
            }
        } else {
            askForPermissions()
        }


    }

    var request: Request? = null
    private fun makeCall(maildId: String, password: String) {

//        val mailId = RequestBody.create(MediaType.parse("multipart/form-data"), maildId)
//        val password = RequestBody.create(MediaType.parse("multipart/form-data"), password)

        val signModel = AdminLoginModel()
        signModel.email = maildId
        signModel.password = password

        val call: Call<AdminLoginResponseModel> = RetrofitClient.getAPIInterface().signIn(signModel)
        request = RetrofitRequest(call, object : ResponseListener<AdminLoginResponseModel?> {
            override fun onResponse(response: AdminLoginResponseModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("ADMINLogin", "response " + response)

                val dataProccessor = SharePreferenceUtil(this@AdminLoginActivity)
                if (response!!.status) {

                    if (response!!.result!!.userType!!.equals("SUPER_ADMIN")) {
                        val intent =
                            Intent(this@AdminLoginActivity, SuperAdminListActivity::class.java)
                        startActivity(intent)
                        dataProccessor.setStr("type", "superAdmin")
                        dataProccessor.setStr("token", response.result!!.token)
                        finish()
                    } else {

                        dataProccessor.setStr("token", response.result!!.token)
                        dataProccessor.setStr("type", "admin")
                        dataProccessor.setStr("profilePhoto", response.result!!.profilePhoto)
                        dataProccessor.setStr("orgName", response.result!!.orgName)
                        val intent = Intent(this@AdminLoginActivity, CreatEventActivity::class.java)
                        startActivity(intent)
                        finish()
                    }

                } else {
                    Util.showCustomAlert(
                        this@AdminLoginActivity, response!!.errors.get(0).message, "Error",
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }
            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("ADMINLogin", "error " + error)
                Util.showCustomAlert(
                    this@AdminLoginActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    fun isPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun askForPermissions(): Boolean {
        if (!isPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) || ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.CAMERA
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                    7998
                )
            }
            return false
        }
        return true
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", getPackageName(), null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }
}