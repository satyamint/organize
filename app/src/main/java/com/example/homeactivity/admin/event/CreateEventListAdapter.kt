package com.example.homeactivity.admin.event


import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.R
import com.example.homeactivity.callbacks.OnItemClickListener
import java.util.*

class CreateEventListAdapter(
    private val activity: Activity,
    val listener: OnItemClickListener<Result>,
    val showRemove: Boolean
) :
    RecyclerView.Adapter<CreateEventListAdapter.ViewHolder>() {
    private val listItems = ArrayList<Result>()


    class ViewHolder(itemView: View, ViewType: Int) : RecyclerView.ViewHolder(itemView) {


        internal lateinit var tvRemove: TextView
        internal lateinit var tvDate: TextView
        internal lateinit var tvTime: TextView
        internal lateinit var txtName: TextView
        internal lateinit var tvMatchType: TextView
        internal lateinit var tvEdit: TextView
        internal lateinit var gameCard: CardView
        internal lateinit var gameImage: ImageView
        internal lateinit var imgTag: ImageView
        internal lateinit var img_tag_inactive: ImageView


        init {
            txtName = itemView.findViewById(R.id.text)
            tvMatchType = itemView.findViewById(R.id.tvMatchType)
            gameCard = itemView.findViewById(R.id.gameCard)
            gameImage = itemView.findViewById(R.id.game_image)
            tvEdit = itemView.findViewById(R.id.tvEdit)
            tvRemove = itemView.findViewById(R.id.tvRemove)
            tvTime = itemView.findViewById(R.id.tvTime)
            tvDate = itemView.findViewById(R.id.tvDate)
            imgTag = itemView.findViewById(R.id.img_tag)
            img_tag_inactive = itemView.findViewById(R.id.img_tag_inactive)
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, posItem: Int) {


        holder.txtName.text = listItems.get(posItem).name
        holder.tvDate.text = listItems!!.get(posItem).startDateTime!!.split("-")[0]
        holder.tvTime.text = listItems!!.get(posItem).startDateTime!!.split("-")[1]

        holder.tvMatchType.setTextColor(
            ContextCompat.getColor(
                activity,
                bgColor(holder.adapterPosition)
            )
        )

        if (listItems.get(posItem).isSlotAvailable!!) {
            holder.img_tag_inactive.visibility = View.INVISIBLE
            holder.imgTag.visibility = View.VISIBLE
        } else {
            holder.img_tag_inactive.visibility = View.VISIBLE
            holder.imgTag.visibility = View.INVISIBLE
        }

        holder.gameImage.setImageResource(getmatchType(listItems.get(posItem).matchType!!))

        holder.tvMatchType.setText(listItems.get(posItem).matchType)
        holder.gameCard.setBackgroundResource(getGradient(holder.adapterPosition))
        holder.gameCard.setOnClickListener(View.OnClickListener {
            if (listItems.get(posItem).isSlotAvailable!!) {
                val position = holder.adapterPosition
                listener.onItemClick(listItems.get(position), it, position)
            }
        })
        holder.tvEdit.setOnClickListener(View.OnClickListener {

            val position = holder.adapterPosition
            listener.onItemClick(listItems.get(position), it, position)
        })
        holder.tvRemove.setOnClickListener(View.OnClickListener {

            val position = holder.adapterPosition
            listener.onItemClick(listItems.get(position), it, position)
        })
        if (showRemove) {

            holder.tvEdit.setText("Edit")
            holder.tvRemove.setText("Remove")
            holder.tvRemove.visibility = View.VISIBLE
        } else {
            holder.tvEdit.setText("View")
            holder.tvRemove.setText("")
            holder.tvRemove.visibility = View.GONE
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.create_event_item, parent, false) //Inflating the layout
        return ViewHolder(v, viewType)
    }

    override fun getItemCount(): Int {
        return listItems.size

    }

    fun setList(drawerList: ArrayList<Result>) {
        listItems.clear()
        listItems.addAll(drawerList)
        notifyDataSetChanged()
    }


    fun addData(item: Result) {
        listItems.add(item)
        notifyDataSetChanged()
    }

    private fun getGradient(pos: Int): Int {

        when (pos % 7) {
            0 ->
                return R.drawable.one_gradient

            1 ->
                return R.drawable.two_gradient
            2 ->
                return R.drawable.three_gradient
            3 ->
                return R.drawable.four_gradient
            4 ->
                return R.drawable.five_gradient
            5 ->
                return R.drawable.six_gradient

            else -> {

                return R.drawable.seven_gradient
            }

        }


    }

    private fun bgColor(pos: Int): Int {

        when (pos % 7) {
            0 ->
                return R.color.oneg

            1 ->
                return R.color.twog
            2 ->
                return R.color.threeg
            3 ->
                return R.color.fourg
            4 ->
                return R.color.fiveg
            5 ->
                return R.color.sixg

            else -> {

                return R.color.seveng
            }

        }


    }


    private fun getmatchType(getmatchType: String): Int {

        if (getmatchType.toLowerCase().equals("solo")) {

            return R.drawable.solo
        } else if (getmatchType.toLowerCase().equals("duo")) {
            return R.drawable.duo33
        } else {
            return R.drawable.squad5
        }

    }
}