package com.example.viewpagerexample

class RoundModel {
    var roundName: String? = null
    var groupList: ArrayList<Group>? = null
    var matchList: ArrayList<Match>? = null
}

class Group {
    var groupName: String? = null

}

class Match {
    var mapName: String? = null
    var mapDateTime: String? = null
    var contactName: String? = null
    var contactNumber: String? = null

}
