package com.example.homeactivity.admin.account.adminprofile

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.example.homeactivity.R
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.example.homeactivity.utility.Util.Companion.hideKeyboardFrom
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import okhttp3.Headers
import retrofit2.Call


lateinit var etYoutube1: TextView
lateinit var etOther1: TextView
lateinit var etInstagram1: TextView
lateinit var etDiscord1: TextView
lateinit var linBack1: LinearLayout
lateinit var img1: AppCompatImageView
lateinit var tvName1: EditText
lateinit var tvOrgName1: TextView
lateinit var tvEmail1: TextView
lateinit var tvPhone1: EditText
lateinit var tvGameId1: TextView
lateinit var progressBar1: ProgressBar
private var relBack: RelativeLayout? = null
private var dataProccessor1: SharePreferenceUtil? = null
lateinit var mAdView1: AdView

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        MobileAds.initialize(this) {}
        etYoutube1 = findViewById(R.id.etYoutube)
        tvOrgName1 = findViewById(R.id.tvOrgName)
        etOther1 = findViewById(R.id.etOther)
        progressBar1 = findViewById(R.id.progressBar)
        etInstagram1 = findViewById(R.id.etInstagram)
        etDiscord1 = findViewById(R.id.etDiscord)
        linBack1 = findViewById(R.id.linBack)
        tvName1 = findViewById(R.id.tvName)
        tvEmail1 = findViewById(R.id.tvEmail)
        tvPhone1 = findViewById(R.id.tvPhone)
        tvGameId1 = findViewById(R.id.tvGameId)
        img1 = findViewById(R.id.img)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        relBack = findViewById(R.id.relBack)

        dataProccessor1 = SharePreferenceUtil(this@ProfileActivity)
        relBack!!.setOnClickListener() {
            finish()
        }

        linBack1.setOnClickListener {
            intent.putExtra("needToUpdate", false)
            setResult(RESULT_OK, intent)
            finish()
        }
        getProfileDetails(intent.getStringExtra("id").toString())
        img1.setOnClickListener {
            openDilogForFullScreen(it)
        }
        hideKeyboardFrom(img1)
    }

    var request: Request? = null
    private fun getProfileDetails(token: String) {

        progressBar1.visibility = View.VISIBLE
        val call: Call<AdminProfileModel> = RetrofitClient.getAPIInterface().getProfilleAdmin(token)
        request = RetrofitRequest(call, object : ResponseListener<AdminProfileModel?> {
            override fun onResponse(response: AdminProfileModel?, headers: Headers?) {

                Log.d("getList", "response " + response)
                progressBar1.visibility = View.GONE
                if (response!!.statusCode == 200) {
                    Glide.with(this@ProfileActivity)
                        .load(response!!.result!!.profilePhoto)
                        .into(img1)
                    tvName1.setText(response.result!!.name)
                    tvEmail1.setText(response.result!!.email)
                    tvPhone1.setText(response.result!!.phone)

                    etYoutube1.setText(response.result!!.youtube)
                    etInstagram1.setText(response.result!!.instagram)
                    etDiscord1.setText(response.result!!.discord)
                    etOther1.setText(response.result!!.otherSocialMediaUrls)
                    tvOrgName1.setText(response.result!!.orgName)
                    tvGameId1.setText(response.result!!.adminGameId)

                } else {
                    Log.d("getList", "error ")
                    Util.showCustomAlert(
                        this@ProfileActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                Log.d("getList", "error " + error)
                progressBar1.visibility = View.GONE
                Util.toast(this@ProfileActivity, "Error")
                finish()
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar1.visibility = View.GONE
                Util.toast(this@ProfileActivity, "Error")
                finish()
            }

        })
        request?.enqueue()
    }

    var alertDialog: AlertDialog? = null
    private fun openDilogForFullScreen(v: View) {


        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.fullscreen_img, null)
        dialogBuilder.setView(dialogView)

        val imageToshow = dialogView.findViewById<View>(R.id.imageToshow) as ImageView

        imageToshow.setImageDrawable((v as AppCompatImageView).drawable)
        val closeBtn = dialogView.findViewById<View>(R.id.closeBtn) as Button

        closeBtn.setOnClickListener {
            alertDialog?.dismiss()


        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }

}