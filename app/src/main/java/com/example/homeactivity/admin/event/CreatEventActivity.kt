package com.example.homeactivity.admin.event

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminlogin.AdminLoginActivity
import com.example.homeactivity.admin.account.adminprofile.AdminProfileActivity
import com.example.homeactivity.admin.event.createquickmatch.add.QuickMatchAddActivity
import com.example.homeactivity.admin.event.createquickmatch.edit.QuickMatchEditActivity
import com.example.homeactivity.animateddilog.PromptDialog
import com.example.homeactivity.callbacks.OnItemClickListener
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.user.userevent.quickMatchDetails.matchlist.QuickMatchListActivity
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.example.viewpagerexample.CreateRoundActivity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.Headers
import retrofit2.Call
import java.util.*


private var drawerList: RecyclerView? = null
private var floatingActionButton: FloatingActionButton? = null
private val listItems = ArrayList<Result>()
private var adapter: CreateEventListAdapter? = null
private lateinit var mDrawerLayout: DrawerLayout
private lateinit var relhamburger: RelativeLayout
private lateinit var progressBar: ProgressBar
private lateinit var tvOrgName: TextView
private lateinit var headerImage: CircleImageView
private lateinit var navigationView: NavigationView
private var dataProccessor: SharePreferenceUtil? = null
lateinit var mAdView1: AdView

class CreatEventActivity : AppCompatActivity(), OnItemClickListener<Result> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creat_event)
        MobileAds.initialize(this) {}
        drawerList = findViewById<View>(R.id.left_drawer) as RecyclerView
        mDrawerLayout = findViewById(R.id.drawer_layout)

        progressBar = findViewById(R.id.progressBar)
        relhamburger = findViewById(R.id.relhamburger)
        floatingActionButton = findViewById<View>(R.id.floatingActionButton) as FloatingActionButton
         navigationView= findViewById(R.id.nav_view)
        progressBar.visibility = View.GONE
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        val headerView = navigationView.getHeaderView(0)
        tvOrgName = headerView.findViewById<View>(R.id.tvOrgName) as TextView
        headerImage = headerView.findViewById<View>(R.id.headerImage) as CircleImageView

        dataProccessor = SharePreferenceUtil(this@CreatEventActivity)

        setDrawer()
        listAdapter()


        relhamburger.setOnClickListener {
            mDrawerLayout.openDrawer(GravityCompat.START)
        }


        floatingActionButton!!.setOnClickListener {

            if (isPermissionsAllowed()) {
                val intent = Intent(this@CreatEventActivity, QuickMatchAddActivity::class.java)
                intent.putExtra("type", "Add")
                resultLauncher.launch(intent)
            } else {
                askForPermissions()
            }

        }
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers()

            // Handle navigation view item clicks here.
            when (menuItem.itemId) {

                R.id.nav_profile -> {
                    val intent = Intent(this@CreatEventActivity, AdminProfileActivity::class.java)
                    resultLauncher.launch(intent)
                }

                R.id.nav_logout -> {
                    Toast.makeText(this, "Logout", Toast.LENGTH_LONG).show()
                    val dataProccessor = SharePreferenceUtil(this@CreatEventActivity)
                    dataProccessor.setStr("token", "")
                    dataProccessor.setStr("type", "")
                    dataProccessor.setStr("profilePhoto", "")
                    dataProccessor.setStr("orgName", "")
                    val intent = Intent(this@CreatEventActivity, AdminLoginActivity::class.java)
                    startActivity(intent)
                    finish()

                }
            }
            // Add code here to update the UI based on the item selected
            // For example, swap UI fragments here

            true
        }
        val res = askForPermissions()
        if (res) {
            getList(dataProccessor!!.getStr("token"))
        }
    }

    private fun setDrawer() {
        val profilePhoto = dataProccessor!!.getStr("profilePhoto")
        val orgName = dataProccessor!!.getStr("orgName")


        Picasso.get()
            .load(profilePhoto)
            .into(headerImage);
        tvOrgName.setText("" + orgName)

        mDrawerLayout.invalidate()
    }

    var request: Request? = null
    private fun getList(token: String) {

        progressBar.visibility = View.VISIBLE
        val call: Call<EventModel> = RetrofitClient.getAPIInterface().getEventList(token)
        request = RetrofitRequest(call, object : ResponseListener<EventModel?> {
            override fun onResponse(response: EventModel?, headers: Headers?) {

                progressBar!!.visibility = View.GONE
                Log.d("getList", "response " + response)

                if (response!!.statusCode == 200) {
                    listItems.clear()
                    listItems.addAll(response!!.resultList!!)
                    adapter!!.setList(listItems)

                } else {
                    Log.d("getList", "error ")
                    Util.showCustomAlert(
                        this@CreatEventActivity,
                        "" + response!!.statusCode,
                        "" + response!!.status,
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }

            }

            override fun onError(status: Int, error: String?) {
                progressBar!!.visibility = View.GONE
                Log.d("getList", "error " + error)
                Util.showCustomAlert(
                    this@CreatEventActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {
                progressBar!!.visibility = View.GONE
            }

        })
        request?.enqueue()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    var alertDialog: AlertDialog? = null

    private fun openDilog(result: Result) {


        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.remove_eventpop_up, null)
        dialogBuilder.setView(dialogView)


        val btnOk = dialogView.findViewById<View>(R.id.btnOk) as Button
        val btnCancel = dialogView.findViewById<View>(R.id.btnCancel) as Button

        btnCancel.setOnClickListener {
            alertDialog?.dismiss()


        }
        btnOk.setOnClickListener {
            alertDialog?.dismiss()
            callRemove(result)

        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }


    private fun listAdapter() {
        drawerList?.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 1)
        drawerList?.layoutManager = layoutManager
        adapter = CreateEventListAdapter(this@CreatEventActivity, this, true)
        drawerList?.adapter =
            adapter

        adapter!!.setList(listItems)

    }

    override fun onItemClick(t: Result, view: View, position: Int) {

//        if (t.eventType.equals("QuickMatch")) {
        if (view.getTag().equals("Edit")) {
            val intent = Intent(this@CreatEventActivity, QuickMatchEditActivity::class.java)
            intent.putExtra("type", "Edit")
            intent.putExtra("Resultobj", t)
            intent.putExtra("ContactDetails", t.contactDetails)
            intent.putParcelableArrayListExtra("listResult", t.matchList)
            resultLauncher.launch(intent)
        } else if (view.getTag().equals("Remove")) {

//            callRemove(t)
            openDilog(t)

        } else {
            val intent = Intent(this@CreatEventActivity, QuickMatchListActivity::class.java)
            intent.putExtra("Resultobj", t)
            intent.putExtra("ContactDetails", t.contactDetails)
            intent.putParcelableArrayListExtra("listResult", t.matchList)
            startActivity(intent)
        }
//        } else {
//            openDilog(view)
//        }

    }

    private fun callRemove(result: Result) {

        progressBar.visibility = View.VISIBLE

        val call: Call<RemoveEventModel> =
            RetrofitClient.getAPIInterface().removeEvent(dataProccessor!!.getStr("token"), result)
        request = RetrofitRequest(call, object : ResponseListener<RemoveEventModel?> {
            override fun onResponse(response: RemoveEventModel?, headers: Headers?) {
                progressBar.visibility = View.GONE

                Log.d("TAG", "callRemove " + response)
                Toast.makeText(
                    this@CreatEventActivity,
                    "Removed  ",
                    Toast.LENGTH_SHORT
                ).show()

                if (response!!.status) {

                    dataProccessor = SharePreferenceUtil(this@CreatEventActivity)
                    getList(dataProccessor!!.getStr("token"))


                } else {
                    Util.showCustomAlert(
                        this@CreatEventActivity, "Error " + response.statusCode, "Error",
                        PromptDialog.DIALOG_TYPE_WRONG
                    )
                }
            }

            override fun onError(status: Int, error: String?) {
                progressBar.visibility = View.GONE
                Log.d("TAG", "callRemove error " + error)
                Util.showCustomAlert(
                    this@CreatEventActivity,
                    error,
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

            override fun onFailure(throwable: Throwable?) {

                progressBar.visibility = View.GONE
                Log.d("TAG", "callRemove onFailure ")
                Util.showCustomAlert(
                    this@CreatEventActivity,
                    "Failure",
                    "Error",
                    PromptDialog.DIALOG_TYPE_WRONG
                )
            }

        })
        request?.enqueue()
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) {
                    setDrawer()
                    getList(dataProccessor!!.getStr("token"))
                }
            }
        }

    fun isPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun askForPermissions(): Boolean {
        if (!isPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) || ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.CAMERA
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                    7998
                )
            }
            return false
        }
        return true
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", getPackageName(), null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }
}