package com.example.homeactivity.admin.event.createquickmatch.add

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import com.example.homeactivity.R
import com.example.homeactivity.admin.account.adminreg.AdminResponseModel
import com.example.homeactivity.admin.event.createquickmatch.ContactDetails
import com.example.homeactivity.admin.event.createquickmatch.MatchDetails
import com.example.homeactivity.admin.event.createquickmatch.QuickMatchAddModel
import com.example.homeactivity.callbacks.ResponseListener
import com.example.homeactivity.retrofit.Request
import com.example.homeactivity.retrofit.RetrofitClient
import com.example.homeactivity.retrofit.RetrofitRequest
import com.example.homeactivity.utility.FileUtils
import com.example.homeactivity.utility.SharePreferenceUtil
import com.example.homeactivity.utility.Util
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


private var addMatchBtn: Button? = null
private var matchLinLay: LinearLayout? = null
private var gameCard: CardView? = null
private lateinit var etMatchDetails: EditText
private lateinit var etYotubeLink: EditText
private lateinit var etPhNo: EditText
private lateinit var matchNotxt: TextView
private lateinit var img1: AppCompatImageView
private lateinit var btnSubmit: Button
private lateinit var tvDate: TextView
private lateinit var tvHour: TextView
private lateinit var tvMapName: TextView
private lateinit var linBack: LinearLayout
private var quickMatchObj: QuickMatchAddModel? = null
lateinit var radioGP: RadioGroup
lateinit var spinnerLang1: Spinner
lateinit var spinnerLang2: Spinner
private var matchTypeSpinner: Spinner? = null
var request: Request? = null
private var multiPartImageList: MutableList<MultipartBody.Part> =
    mutableListOf<MultipartBody.Part>()
private var progressBar: ProgressBar? = null
private var relRemovePhoto: RelativeLayout? = null
private var relAddPhoto: RelativeLayout? = null
private var switch1: Switch? = null
var hashMap: HashMap<String, Int> = HashMap<String, Int>()
lateinit var mAdView1: AdView

class QuickMatchAddActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_match_add)
        MobileAds.initialize(this) {}
        addMatchBtn = findViewById<View>(R.id.addMatchBtn) as Button
        matchLinLay = findViewById<View>(R.id.matchLinLay) as LinearLayout
        val removeTxt = findViewById<View>(R.id.removeTxt) as TextView
        gameCard = findViewById<View>(R.id.gameCard) as CardView
        etMatchDetails = findViewById(R.id.etMatchDetails)
        etYotubeLink = findViewById(R.id.etYotubeLink)
        matchTypeSpinner = findViewById<Spinner>(R.id.gameTypeSpinner)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        etPhNo = findViewById(R.id.etPhNo)
        relRemovePhoto = findViewById(R.id.relRemovePhoto)
        tvDate = findViewById(R.id.tvDate)
        tvHour = findViewById<View>(R.id.tvHour) as TextView
        img1 = findViewById(R.id.img1)
        linBack = findViewById(R.id.linBack)
        relAddPhoto = findViewById(R.id.relAddPhoto)
        matchNotxt = findViewById<View>(R.id.matchNotxt) as TextView
        removeTxt.visibility = View.INVISIBLE
        radioGP = findViewById(R.id.radioGP)
        btnSubmit = findViewById(R.id.btnSubmit)
        tvMapName = findViewById(R.id.tvMapName)
        spinnerLang1 = findViewById<View>(R.id.spinnerLang1) as Spinner
        spinnerLang2 = findViewById<View>(R.id.spinnerLang2) as Spinner

        switch1 = findViewById(R.id.switch1)
        quickMatchObj = QuickMatchAddModel()
        var contactObj = ContactDetails()
        contactObj.mobileNumber = ""
        contactObj.name = ""
        quickMatchObj!!.contactDetails = contactObj

        quickMatchObj?.rules = ""
        quickMatchObj?.youtubeLink = ""
        quickMatchObj?.gameType = "Quick Match"
        quickMatchObj?.isPaid = true
        clickListeners()

//        addMatchCard()
        matchInfoFreez(true)

        switch1!!.isChecked = true

        Util.hideKeyboardFrom(etMatchDetails)
        switch1!!.setOnCheckedChangeListener { _, isChecked ->

        }


    }

    private fun clickListeners() {

        matchTypeSpinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                quickMatchObj!!.matchType = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
        spinnerLang1!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                quickMatchObj!!.primaryLanguage = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        spinnerLang2!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                quickMatchObj!!.secondaryLanguage = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        etMatchDetails.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                quickMatchObj?.rules = s.toString()
            }

        })
        etPhNo.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                quickMatchObj!!.contactDetails!!.mobileNumber = s.toString()
            }

        })
        etYotubeLink.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                quickMatchObj?.youtubeLink = s.toString()
            }

        })
        linBack!!.setOnClickListener {
            val intent = Intent()
            intent.putExtra("needToUpdate", false)
            setResult(RESULT_OK, intent)
            finish()
        }
        relRemovePhoto!!.setOnClickListener {

            img1.setImageDrawable(null)
            if (hashMap.get("coverImage") != null) {
                hashMap.get("coverImage")?.let { it1 -> multiPartImageList.removeAt(it1) }
            }
        }
        btnSubmit!!.setOnClickListener {

            if (etMatchDetails.text.toString().isEmpty()) {
                etMatchDetails.setError("Cannot be Empty")
                Util.toast(this@QuickMatchAddActivity, "Match Details cannot be empty")
                return@setOnClickListener
            }
            if (etPhNo.text.toString().isEmpty()) {
                etPhNo.setError("Cannot be Empty")
                Util.toast(this@QuickMatchAddActivity, "Phone Number cannot be empty")

                return@setOnClickListener
            }
            if (quickMatchObj!!.matchList== null) {
                Util.toast(this@QuickMatchAddActivity, "Add Atleast One Match")

                return@setOnClickListener
            }
            if (quickMatchObj!!.matchList!!.size<=0) {
                Util.toast(this@QuickMatchAddActivity, "Add Atleast One Match")

                return@setOnClickListener
            }


            progressBar!!.visibility = View.VISIBLE
            val genid: Int = radioGP.getCheckedRadioButtonId()
            val radioButton = findViewById<View>(genid) as RadioButton
            val typee = radioButton.text.toString()

            quickMatchObj!!.isPaid = typee.equals("Paid")
            val gson = Gson()
            val json = gson.toJson(quickMatchObj!!.matchList)
            Log.d("TAG", "matchList---> " + json)

            Log.d(
                "TAG",
                "---> typee " + typee + " spinnerLang1- " + spinnerLang1!!.selectedItem.toString() + " spinnerLang2 " +
                        spinnerLang2!!.selectedItem.toString()
            )

            var wholeListMap = HashMap<String, RequestBody>()

            val gsonObj = Gson()
            val contactDetails = gsonObj.toJson(quickMatchObj!!.contactDetails)


            Log.d(
                "TAG",
                "---> contactDetails " + contactDetails
            )

            wholeListMap.put(
                "name",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "Quick Match "
                )
            )


            wholeListMap.put(
                "quickMatchResult",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""
                )
            )
            wholeListMap.put(
                "contactDetails",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "" + contactDetails
                )
            )
            wholeListMap.put(
                "matchNo",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "0"
                )
            )
            wholeListMap.put(
                "matchList",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    json
                )
            )
            wholeListMap.put(
                "isPaid",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "" + quickMatchObj!!.isPaid
                )
            )
            wholeListMap.put(
                "isSlotAvailable",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "" + switch1!!.isChecked
                )
            )
            wholeListMap.put(
                "matchType",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    quickMatchObj!!.matchType
                )
            )
            wholeListMap.put(
                "rules",
                RequestBody.create(MediaType.parse("multipart/form-data"), quickMatchObj!!.rules)
            )
            wholeListMap.put(
                "matchImage",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "" + quickMatchObj!!.matchImage
                )
            )
            wholeListMap.put(
                "youtubeLink",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    "" + quickMatchObj!!.youtubeLink
                )
            )
            wholeListMap.put(
                "gameType",
                RequestBody.create(MediaType.parse("multipart/form-data"), quickMatchObj!!.gameType)
            )
            wholeListMap.put(
                "primaryLanguage",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    quickMatchObj!!.primaryLanguage
                )
            )
            wholeListMap.put(
                "secondaryLanguage",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    quickMatchObj!!.secondaryLanguage
                )
            )

            if (quickMatchObj!!.matchList != null && quickMatchObj!!.matchList!!.size > 0)
                wholeListMap.put(
                    "startDateTime",
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        quickMatchObj!!.matchList!!.get(0).date
                    )
                )
            if (quickMatchObj!!.matchList != null && quickMatchObj!!.matchList!!.size > 0)
                wholeListMap.put(
                    "createdDateTime",
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        quickMatchObj!!.matchList!!.get(0).date
                    )
                )


            val gsonObj1 = Gson()
            val matchList = gsonObj1.toJson(quickMatchObj!!.matchList!!)
            Log.d(
                "TAG",
                "---> matchList " + matchList
            )
            wholeListMap.put(
                "matchList",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    matchList
                )
            )
            val str_date = ""+quickMatchObj!!.matchList!!.get(quickMatchObj!!.matchList!!.size-1).date!!.split("-")[0]
            Log.d("TAG","TIME str_date "+str_date)
            val formatter: DateFormat = SimpleDateFormat("ddMMMyyyy")
            val date = formatter.parse(str_date) as Date

            Log.d("TAG","TIME "+date.time)

            wholeListMap.put(
                "ttl",
                RequestBody.create(
                    MediaType.parse("multipart/form-data"),
                    ""+(date.time)
                )
            )

            val dataProccessor = SharePreferenceUtil(this@QuickMatchAddActivity)

//            var value=1
//            for (fileImage in filePathimage!!) {
//                val requestFile: RequestBody =
//                    RequestBody.create(
//                        MediaType.parse("multipart/form-data"),
//                        fileImage
//                    )
//                value++
//                val image = MultipartBody.Part.createFormData("image key of match list",fileImage.getName(), requestFile)
//
//                multiPartImageList.add(image)
//            }


            val call: Call<AdminResponseModel> = RetrofitClient.getAPIInterface()
                .createMatch(
                    dataProccessor.getStr("token"),
                    wholeListMap, multiPartImageList
                )
            request = RetrofitRequest(call, object : ResponseListener<AdminResponseModel?> {
                override fun onResponse(response: AdminResponseModel?, headers: Headers?) {
                    progressBar!!.visibility = View.GONE
                    Log.d("ADMINREG", "response " + response)



                    if (response!!.status.equals(true)) {
                        val intent = Intent()
                        intent.putExtra("needToUpdate", true)
                        setResult(RESULT_OK, intent)
                        finish()
                    }

                }

                override fun onError(status: Int, error: String?) {
                    progressBar!!.visibility = View.GONE
                    Log.d("ADMINREG", "error " + error)
                    Toast.makeText(
                        this@QuickMatchAddActivity,
                        "Error please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(throwable: Throwable?) {
                    progressBar!!.visibility = View.GONE
                }

            })
            request?.enqueue()


        }


        gameCard?.setOnClickListener() {
            callGameDetails(matchNotxt)
        }


        addMatchBtn?.setOnClickListener() {

            if (tvMapName.text.equals("Select Game")) {
                Toast.makeText(
                    this,
                    "First Fill match details then you can create another match",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                if ((matchLinLay as ViewGroup).childCount > 0) {
                    val nextChild =
                        (matchLinLay as ViewGroup).getChildAt((matchLinLay as ViewGroup).childCount - 1)
                    val tvMapName = nextChild.findViewById<View>(R.id.tvMapName) as TextView
                    if (tvMapName.text.equals("Select Game")) {
                        Toast.makeText(
                            this,
                            "First Fill match details then you can create another match",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        addMatchCard()
                    }
                } else {
                    addMatchCard()
                }
            }

        }
        relAddPhoto?.setOnClickListener() {
            ImagePicker.with(this)
                .crop()
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
        }
        img1?.setOnClickListener() {
            openDilogForFullScreen(it)
        }

    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data!!
                val fileObj = FileUtils.getFile(this@QuickMatchAddActivity, fileUri)

                Picasso.get()
                    .load(fileObj)
                    .into(img1)

                val requestFile: RequestBody =
                    RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        fileObj
                    )
                val image = MultipartBody.Part.createFormData(
                    "coverImage",
                    fileObj.getName(),
                    requestFile
                )
                hashMap.put("coverImage", multiPartImageList.size)
                multiPartImageList.add(image)
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    private fun addMatchCard() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView: View = inflater.inflate(R.layout.quickmatch_add, null)

        val matchNotxt = rowView.findViewById<View>(R.id.matchNotxt) as TextView
        val removeTxt = rowView.findViewById<View>(R.id.removeTxt) as TextView
        val gameCard = rowView.findViewById<View>(R.id.gameCard) as CardView
        val tvDate = rowView.findViewById<View>(R.id.tvDate) as TextView
        val tvHour = rowView.findViewById<View>(R.id.tvHour) as TextView
        val tvMapName = rowView.findViewById<View>(R.id.tvMapName) as TextView

        gameCard.setOnClickListener() {
            callGameDetails(matchNotxt)
        }
        removeTxt.setOnClickListener() {

            val matchNotxtRemove =
                (removeTxt.tag as View?)!!.findViewById<View>(R.id.matchNotxt) as TextView
            if (Integer.parseInt(
                    matchNotxtRemove.text.toString()
                ) <= quickMatchObj!!.matchList!!.size
            ) {
                quickMatchObj!!.matchList!!.remove(
                    quickMatchObj!!.matchList!!.get(
                        Integer.parseInt(
                            matchNotxtRemove.text.toString()
                        ) - 1
                    )
                )
            }
            for (index in 0 until quickMatchObj!!.matchList!!.size) {
                quickMatchObj!!.matchList!!.get(index).matchNo = "" + (index + 1)
            }
            matchLinLay?.removeView(removeTxt.tag as View?)
            for (index in 0 until (matchLinLay as ViewGroup).childCount) {
                val nextChild = (matchLinLay as ViewGroup).getChildAt(index)
                val matchNotxt = nextChild.findViewById<View>(R.id.matchNotxt) as TextView
                matchNotxt.text = "" + (index + 2)
            }


        }
        removeTxt.tag = (rowView)
        matchNotxt.text = "" + (matchLinLay?.childCount?.plus(2))
        matchLinLay?.addView(rowView)
    }

    private fun matchInfoFreez(enablEdit: Boolean) {

        etMatchDetails.isEnabled = enablEdit
        etYotubeLink.isEnabled = enablEdit
        if (enablEdit) {
            addMatchBtn!!.visibility = View.VISIBLE
        } else
            addMatchBtn!!.visibility = View.GONE

        for (index in 0 until (matchLinLay as ViewGroup).childCount) {
            val nextChild = (matchLinLay as ViewGroup).getChildAt(index)
            val removeTxt = nextChild.findViewById<View>(R.id.removeTxt) as TextView
            if (enablEdit) {
                removeTxt!!.visibility = View.VISIBLE
            } else
                removeTxt!!.visibility = View.GONE
        }


    }

    private fun callGameDetails(matchNotxt: TextView) {

        val intent = Intent(this, FillQuickmatchDetails::class.java)
        intent.putExtra("matchNo", matchNotxt.text.toString())
        intent.putExtra("matchType", quickMatchObj!!.matchType)
        intent.putExtra("contactDetails", quickMatchObj!!.contactDetails)
        if (quickMatchObj!!.matchList != null && quickMatchObj!!.matchList!!.size >= (Integer.parseInt(
                matchNotxt.text.toString()
            ))
        )
            intent.putExtra(
                "Matchobject",
                quickMatchObj!!.matchList?.get(Integer.parseInt(matchNotxt.text.toString()) - 1)
            )
        startForActivityResult.launch(intent)

    }

    private val startForActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            if (resultCode == Activity.RESULT_OK && data != null) {
                val matchDetails = data.getParcelableExtra<MatchDetails>("MatchDetails")

                if (matchDetails != null) {

                    if (quickMatchObj!!.matchList == null)
                        quickMatchObj!!.matchList = arrayListOf<MatchDetails>()

                    if ((Integer.parseInt(matchDetails!!.matchNo)) <= quickMatchObj!!.matchList!!.size) {
                        quickMatchObj!!.matchList!!.set(
                            (Integer.parseInt(matchDetails!!.matchNo) - 1),
                            matchDetails
                        )
                    } else {
                        quickMatchObj!!.matchList!!.add(
                            (Integer.parseInt(matchDetails!!.matchNo) - 1),
                            matchDetails
                        )
                    }


                    if (quickMatchObj!!.matchList!!.size == 1) {

                        firstMatchcard(quickMatchObj!!.matchList!!.get(0))
                    } else if (quickMatchObj!!.matchList!!.size <= ((matchLinLay as ViewGroup).childCount + 1)) {

                        firstMatchcard(quickMatchObj!!.matchList!!.get(0))
                        for (index in 0 until quickMatchObj!!.matchList!!.size - 1) {
                            val nextChild = (matchLinLay as ViewGroup).getChildAt(index)
                            val matchNotxt =
                                nextChild.findViewById<View>(R.id.matchNotxt) as TextView
                            val tvDate = nextChild.findViewById<View>(R.id.tvDate) as TextView
                            val tvHour = nextChild.findViewById<View>(R.id.tvHour) as TextView
                            val tvMapName = nextChild.findViewById<View>(R.id.tvMapName) as TextView
                            matchNotxt.text =
                                "" + quickMatchObj!!.matchList!!.get(index + 1).matchNo

                            if (quickMatchObj!!.matchList!!.get(index + 1).date!!.split("-") != null &&
                                quickMatchObj!!.matchList!!.get(index + 1).date!!.split("-").size > 0
                            ) {
                                tvDate.text =
                                    "" + quickMatchObj!!.matchList!!.get(index + 1).date!!.split("-")[0]
                                tvHour.text =
                                    "" + quickMatchObj!!.matchList!!.get(index + 1).date!!.split("-")[1]
                            }

                            tvMapName.text = "" + quickMatchObj!!.matchList!!.get(index + 1).mapName

                        }

                    } else {
                        Toast.makeText(
                            this,
                            " Error ",
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } else {


                }
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    private fun firstMatchcard(matchDetails: MatchDetails) {

        matchNotxt.text = "" + matchDetails.matchNo
        if (matchDetails.date!!.split("-") != null &&
            matchDetails.date!!.split("-").size > 0
        ) {
            tvDate.text = "" + matchDetails.date!!.split("-")[0]
            tvHour.text = "" + matchDetails.date!!.split("-")[1]
        }

        tvMapName.text = "" + matchDetails.mapName
    }
    var alertDialog: AlertDialog? = null
    private fun openDilogForFullScreen(v: View) {


        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.fullscreen_img, null)
        dialogBuilder.setView(dialogView)

        val imageToshow = dialogView.findViewById<View>(R.id.imageToshow) as ImageView

        imageToshow.setImageDrawable((v as AppCompatImageView).drawable)
        val closeBtn = dialogView.findViewById<View>(R.id.closeBtn) as Button

        closeBtn.setOnClickListener {
            alertDialog?.dismiss()


        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }


}