package com.example.homeactivity.admin.event.createquickmatch.add

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.R
import com.example.homeactivity.admin.event.createquickmatch.ContactDetails
import com.example.homeactivity.admin.event.createquickmatch.MatchDetails
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*


private var tvM: TextView? = null
private var tvDate: TextView? = null
private var year: Int? = null
private var month: Int? = null
private var day: Int? = null
private var hour: Int? = null
private var minute: Int? = null
private var btnSave: Button? = null
private var matchDetails: MatchDetails? = null
private var tvMatchType: TextView? = null
private var mapSpinner: Spinner? = null
private var etGameDesc: EditText? = null
private var etContactNo: TextView? = null
private var linDateTime: LinearLayout? = null
private var linBack: LinearLayout? = null
private var matchNo: TextView? = null
private var mAdView2: AdView? = null

class FillQuickmatchDetails : AppCompatActivity(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fill_quickmatch_details)
        MobileAds.initialize(this) {}
        tvMatchType = findViewById<TextView>(R.id.tvMatchType)
        mapSpinner = findViewById<Spinner>(R.id.mapSpinner)
        tvDate = findViewById<TextView>(R.id.tvDate)
        tvM = findViewById<TextView>(R.id.tvM)
        matchNo = findViewById<TextView>(R.id.matchNo)
        btnSave = findViewById<Button>(R.id.btnSave)
        etGameDesc = findViewById<EditText>(R.id.etGameDesc)
        etContactNo = findViewById<TextView>(R.id.etContactNo)
        linDateTime = findViewById(R.id.linDateTime)
        linBack = findViewById(R.id.linBack)
        mAdView2 = findViewById<AdView>(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView2!!.loadAd(adRequest1)


        matchDetails = MatchDetails()
        matchDetails!!.matchRulesDesc = ""
        matchDetails!!.date = ""

        matchDetails!!.slotAvilable = false


        val matchNoStr: String = intent.getStringExtra("matchNo").toString()
        val matchType: String = intent.getStringExtra("matchType").toString()
        val matchObject: MatchDetails? = intent.getParcelableExtra<MatchDetails>("Matchobject")
        if (matchObject != null)
            matchDetails = matchObject

        tvMatchType!!.setText(matchType)
        matchNo!!.setText(matchNoStr)
        matchDetails!!.matchNo = matchNoStr

        clickListeners()


        val contactDetails: ContactDetails? =
            intent.getParcelableExtra<ContactDetails>("contactDetails")
        if (contactDetails != null) {
            etContactNo!!.setText(contactDetails!!.mobileNumber)
        }

        if (matchDetails != null) {
            etGameDesc!!.setText(matchDetails!!.matchRulesDesc)
            if (matchDetails!!.date!!.split("-") != null &&
                matchDetails!!.date!!.split("-").size == 2
            ) {
                tvM!!.setText(matchDetails!!.date!!.split("-")[1])
                tvDate!!.setText("" + matchDetails!!.date!!.split("-")[0])
            }

            if (matchDetails!!.mapName!!.toLowerCase().equals("miramar")) {
                mapSpinner!!.setSelection(1)
            } else if (matchDetails!!.mapName!!.toLowerCase().equals("sanhok")) {
                mapSpinner!!.setSelection(2)
            } else if (matchDetails!!.mapName!!.toLowerCase().equals("vikendi")) {
                mapSpinner!!.setSelection(3)
            } else {
                mapSpinner!!.setSelection(0)
            }

        }


        matchDetails!!.matchRulesDesc = etGameDesc!!.text.toString()

    }

    private fun clickListeners() {


        etGameDesc!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null)
                    matchDetails!!.matchRulesDesc = s.toString()
                else
                    matchDetails!!.matchRulesDesc = ""
            }
        })
        mapSpinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos)
                matchDetails!!.mapName = item.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })


        btnSave!!.setOnClickListener {


            val gson = Gson()
            val json = gson.toJson(matchDetails)
            Log.d("TAG", "---> " + json)
            if (matchDetails!!.mapName!!.isEmpty()) {
                val map = mapSpinner!!.getSelectedItem().toString()
                matchDetails!!.mapName = map

            }

            if (matchDetails!!.matchRulesDesc!!.isEmpty()) {

                etGameDesc!!.setError("Cannot be Empty")
            } else if (matchDetails!!.date!!.isEmpty()) {

                Toast.makeText(this, "Please select any date ", Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent()
                intent.putExtra("id", "return value")
                intent.putExtra("MatchDetails", matchDetails)
                setResult(RESULT_OK, intent)
                finish()
            }


        }



        linDateTime!!.setOnClickListener {
            val calendar: Calendar = Calendar.getInstance()
            year = calendar.get(Calendar.YEAR)
            month = calendar.get(Calendar.MONTH)
            day = calendar.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog =
                DatePickerDialog(
                    this@FillQuickmatchDetails, this@FillQuickmatchDetails,
                    year!!, month!!, day!!
                )
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + (24 * 3600000))
            datePickerDialog.show()
            Log.d("TAG","TEST currentTimeMillis "+System.currentTimeMillis())

        }

        linBack!!.setOnClickListener {
            finish()
        }
    }


    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {

        val cal = Calendar.getInstance()
        val month_date = SimpleDateFormat("MMM")
        val monthnum = p2
        cal[Calendar.MONTH] = monthnum

        tvDate!!.setText("" + "" + p3 + "" + month_date.format(cal.time) + p1)
        matchDetails!!.date = "" + p3 + "" + month_date.format(cal.time) + p1
        val c = Calendar.getInstance()
        hour = c[Calendar.HOUR]
        minute = c[Calendar.MINUTE]
        val timePickerDialog = TimePickerDialog(
            this@FillQuickmatchDetails,
            this@FillQuickmatchDetails,
            hour!!,
            minute!!,
            DateFormat.is24HourFormat(this)
        )
        timePickerDialog.show()

    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {

        val s = "$p1:$p2"
        val f1 = SimpleDateFormat("HH:mm") //HH for hour of the day (0 - 23)

        val d: Date = f1.parse(s)
        val hr = SimpleDateFormat("h")
        val min = SimpleDateFormat("mm")
        val amPm = SimpleDateFormat("aa")


        matchDetails!!.date =
            matchDetails!!.date + "-" + ("" + hr.format(d).toLowerCase() + ":" + min.format(d)
                .toLowerCase() + ":" + amPm.format(d).toLowerCase())

        tvM!!.setText(matchDetails!!.date!!.split("-")[1])

    }


}