package com.example.viewpagerexample

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class CreateRoundPagerAdapter(
    private val context: Context, fragment: FragmentActivity
) :
    FragmentStateAdapter(fragment) {


    override fun createFragment(position: Int): Fragment {

        return RoundFragment(roundList.get(position))
    }

    override fun getItemCount(): Int {
        return roundList.size
    }

    val roundList: java.util.ArrayList<RoundModel> = arrayListOf()
    fun addRound(roundModel: RoundModel) {
        roundList.add(roundModel)
        notifyDataSetChanged()

    }

}