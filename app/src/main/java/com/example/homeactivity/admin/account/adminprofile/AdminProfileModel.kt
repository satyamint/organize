package com.example.homeactivity.admin.account.adminprofile

import com.example.homeactivity.retrofit.Error
import com.google.gson.annotations.SerializedName

class AdminProfileModel {

    @SerializedName("errors")
    public lateinit var errors: List<Error>

    @SerializedName("status")
    public val status: Boolean = false

    @SerializedName("statusCode")
    public val statusCode: Int = 0

    @SerializedName("result")
    public val result: Result? = null


}

class Result {
    @SerializedName("_id")
    public val id: String? = null

    @SerializedName("name")
    public val name: String? = null

    @SerializedName("status")
    public val status: String? = null

    @SerializedName("userType")
    var userType: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("orgName")
    var orgName: String? = null

    @SerializedName("adminGameId")
    var adminGameId: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("profilePhoto")
    var profilePhoto: String? = null

    @SerializedName("instagram")
    var instagram: String? = null

    @SerializedName("discord")
    var discord: String? = null

    @SerializedName("youtube")
    var youtube: String? = null

    @SerializedName("otherSocialMediaUrls")
    var otherSocialMediaUrls: String? = null

    @SerializedName("socialMediaLinks")
    var socialMediaLinks: SocialMediaLinks? = null

}

class Ig {
    var handle: String? = null
    var url: String? = null
    var _id: String? = null
}

class Fb {
    var handle: String? = null
    var url: String? = null
    var _id: String? = null
}

class Discord {
    var handle: String? = null
    var url: String? = null
    var _id: String? = null
}

class WhatsApp {
    var handle: String? = null
    var url: String? = null
    var _id: String? = null
}

class Telegram {
    var handle: String? = null
    var url: String? = null
    var _id: String? = null
}

class SocialMediaLinks {
    var ig: Ig? = null
    var fb: Fb? = null
    var discord: Discord? = null
    var whatsApp: WhatsApp? = null
    var telegram: Telegram? = null
}