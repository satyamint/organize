package com.example.homeactivity

import android.app.Application
import com.example.homeactivity.retrofit.RetrofitClient

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        RetrofitClient.create(cacheDir)

    }
}