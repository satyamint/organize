package com.example.homeactivity

import com.google.gson.annotations.SerializedName

public class SplashModel() {


    @SerializedName("statusCode")
    public var statusCode: Int = 0

    @SerializedName("status")
    public var status: Boolean = false

    @SerializedName("result")
    public var result: Result? = null


}

public class Result() {
    @SerializedName("_id")
    public var _id: String? = null


    @SerializedName("latestVersion")
    public var latestVersion: String? = null


}



