package com.example.homeactivity.retrofit

import com.google.gson.annotations.SerializedName

class Error {

    @SerializedName("message")
    public val message: String? = null

    @SerializedName("nextAction")
    public val nextAction: String? = null

    @SerializedName("name")
    public val name: String? = null


}