package com.example.homeactivity.retrofit;

import com.example.homeactivity.callbacks.ResponseListener;
import com.google.gson.Gson;

import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;


public final class RetrofitRequest<T> extends Request {

    private final ResponseListener responseListener;
    private Call<T> call;

    public RetrofitRequest(Call<T> call, ResponseListener<T> responseListener) {
        this.call = call;
        this.responseListener = responseListener;
    }

    @Override
    public void enqueue() {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, retrofit2.Response<T> response) {
                if (response.isSuccessful())
                    responseListener.onResponse(response.body(), response.headers());
                else {

                    if (response.message().isEmpty()) {
                        Gson gson = new Gson();

                        String jsonInString = "";
                        try {
                            jsonInString = response.errorBody().string();


                            ErrResponseModel err = gson.fromJson(jsonInString, ErrResponseModel.class);

                            if (err.getErrors().size() > 0)
                                responseListener.onError(response.code(), err.getErrors().get(0).getMessage());
                            else {
                                responseListener.onError(response.code(), "error list is 0");

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            responseListener.onError(9797, "Exception");
                        }
                    } else {
                        responseListener.onError(response.code(), response.message().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                if (call != null && call.isCanceled())// don't process if request is cancelled.
                    return;
                if (t instanceof UnknownHostException)
                    responseListener.onFailure(t);//network error
                else {
                    responseListener.onError(400, "Something went wrong");
                }
            }
        });
    }

    @Override
    public void cancel() {
        call.cancel();
    }

    @Override
    public void retry() {
        call = call.clone();
        enqueue();
    }

}
