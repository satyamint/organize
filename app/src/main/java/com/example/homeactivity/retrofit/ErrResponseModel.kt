package com.example.homeactivity.retrofit

import com.google.gson.annotations.SerializedName

class ErrResponseModel {

    @SerializedName("errors")
    public lateinit var errors: List<Error>

    @SerializedName("status")
    public val status: Boolean = false


}
