package com.example.homeactivity.retrofit;


import com.example.homeactivity.SplashModel;
import com.example.homeactivity.admin.account.adminlogin.AdminLoginModel;
import com.example.homeactivity.admin.account.adminlogin.AdminLoginResponseModel;
import com.example.homeactivity.admin.account.adminprofile.AdminProfileModel;
import com.example.homeactivity.admin.account.adminreg.AdminResponseModel;
import com.example.homeactivity.admin.event.EventModel;
import com.example.homeactivity.admin.event.RemoveEventModel;
import com.example.homeactivity.admin.event.Result;
import com.example.homeactivity.superadmin.SuperAdminModel;
import com.example.homeactivity.superadmin.SuperAdminUpdateStatus;
import com.example.homeactivity.superadmin.SuperAdminUpdateVersion;
import com.example.homeactivity.superadmin.SuperAdminVerifyResponseModel;
import com.example.homeactivity.user.org.FilterOrgModel;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface APIInterface {
//    @Multipart
//    @POST("login")
//    Call<AdminResponseModel> signInAdmin( @Part("email") RequestBody email,
//                                          @Part("password") RequestBody password);

    @POST("login")
    Call<AdminLoginResponseModel> signIn(@Body AdminLoginModel signinModel);

    @POST("updateAnyUserAsSuperAdmin")
    Call<SuperAdminVerifyResponseModel> verifyAdmin(@retrofit2.http.Header("Authorization") String token,
                                                    @Body SuperAdminUpdateStatus signinModel);

    @POST("updateAppInfo")
    Call<SuperAdminVerifyResponseModel> updateVersion(@retrofit2.http.Header("Authorization") String token,
                                                    @Body SuperAdminUpdateVersion signinModel);

    @Multipart
    @POST("users")
    Call<AdminResponseModel> signUpAdmin(@Part("name") RequestBody name,
                                         @Part("orgName") RequestBody orgname,
                                         @Part("email") RequestBody email,
                                         @Part("phone") RequestBody phone,
                                         @Part("adminGameId") RequestBody gameId,
                                         @Part("password") RequestBody password,
                                         @Part("instagram") RequestBody instagram,
                                         @Part("discord") RequestBody discord,
                                         @Part("youtube") RequestBody youtube,
                                         @Part("otherSocialMediaUrls") RequestBody otherSocialMediaUrls,
                                         @Part MultipartBody.Part image);

    @Multipart
    @POST("updateProfileDetails")
    Call<AdminProfileModel> updateProfile(@retrofit2.http.Header("Authorization") String token,
                                          @Part("name") RequestBody name,
                                          @Part("email") RequestBody email,
                                          @Part("phone") RequestBody phone,
                                          @Part("instagram") RequestBody instagram,
                                          @Part("discord") RequestBody discord,
                                          @Part("youtube") RequestBody youtube,
                                          @Part("otherSocialMediaUrls") RequestBody otherSocialMediaUrls,
                                          @Part("orgName") RequestBody orgName,
                                          @Part MultipartBody.Part image);

    @Multipart
    @POST("game/createGameEvent")
    Call<AdminResponseModel> createMatch(@retrofit2.http.Header("Authorization") String token,
                                         @PartMap() HashMap<String, RequestBody> wholemap,
                                         @Part List<MultipartBody.Part> listimage);

    @Multipart
    @POST("game/updateGameEvent")
    Call<AdminResponseModel> updateMatch(@retrofit2.http.Header("Authorization") String token,
                                         @PartMap() HashMap<String, RequestBody> wholemap,
                                         @Part List<MultipartBody.Part> listimage);

    @GET("game/getMyGameEvents")
    Call<EventModel> getEventList(@retrofit2.http.Header("Authorization") String token);

    @GET("getAppInfo")
    Call<SplashModel> getVersion();

    @GET("game/getAllGameEvents")
    Call<EventModel> getEventList();

    @GET("getUsersByStatus")
    Call<FilterOrgModel> getOrgList(@Query("userStatus") String status, @Query("query") String query);

    @GET("getUsersByStatus")
    Call<SuperAdminModel> getSuperAdminOrgList(@Query("userStatus") String status, @Query("query") String query);

    @GET("getUserByUserId")
    Call<AdminProfileModel> getProfilleAdmin(@Query("userId") String status);

    @GET("game/getGameEventsByUserId")
    Call<EventModel> getUserEventList(@Query("userId") String status);

    @GET("getProfileDetails")
    Call<AdminProfileModel> getProfile(@retrofit2.http.Header("Authorization") String token);

    @POST("game/removeGameEvent")
    Call<RemoveEventModel> removeEvent(@retrofit2.http.Header("Authorization") String token, @Body Result eventModel);

//    @POST("users")
//    Call<AdminResponseModel> registerAdmin(@Body AdminRegModel regModel);

//    @Multipart
//    @POST("user/updateprofile")
//    Call<ResponseBody> updateProfile(@Part("user_id") RequestBody id,
//                                           @Part("full_name") RequestBody fullName,
//                                           @Part MultipartBody.Part image,
//                                           @Part("other") RequestBody other);


//    @FormUrlEncoded
//    @POST("login")
//    Call<SignInResponseModel> signIn(@Field("email") String email, @Field("type") String type, @Field("pass") String pass);

//    @POST("login")
//    Call<SignInResponseModel> signIn(@Body SigninModel signinModel);


    interface Header {
        String AUTHORIZATION = "Authorization";
        String TIMEZONE = "Timezone";

    }

//    @Multipart
//    @POST("patients/register")
//    Call<Response> register(@Part MultipartBody.Part file, @PartMap() Map<String, RequestBody> partMap);


//    @GET("configurations")
//    Call<Configuration> downloadConfiguration();

//    @FormUrlEncoded
//    @POST("patients/get-otp")
//    Call<Response> requestOTP(@Field("uid") String mobileNo);

//    @POST
//    Call<Response> saveMedicineTrackerInfo(@Url String url, @Body SaveMedicineReadingRequestModel medicine);

}
