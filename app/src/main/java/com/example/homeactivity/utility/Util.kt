package com.example.homeactivity.utility

import android.R
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.homeactivity.animateddilog.PromptDialog
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

class Util {
    companion object {

        const val MOBILE_PATTERN = "^(?!0+$)[0-9]{6,14}$"
        fun openWithChrome(context: Context, url: String) {
//            val builder = CustomTabsIntent.Builder()
//            builder.setToolbarColor(ResourcesCompat.getColor(context.resources, R.color.colorPrimary, null))
//            val customTabsIntent = builder.build()
//            customTabsIntent.launchUrl(context, Uri.parse(url))
        }

        fun hideKeyboardFrom(view: View) {
            val imm =
                view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun toast(activity: Activity, str: String) {
            Toast.makeText(activity, str, Toast.LENGTH_SHORT).show()
        }

        fun setUtilBackGround(layout: View, contex: Context, drawable: Int) {
            val sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                layout.setBackgroundDrawable(ContextCompat.getDrawable(contex, drawable));
            } else {
                layout.setBackground(ContextCompat.getDrawable(contex, drawable));
            }
        }

        fun isValidMobileNumber(mobileNumber: String): Boolean {
            if (TextUtils.isEmpty(mobileNumber) || mobileNumber.length != 10) return false
            val pattern = Pattern.compile(MOBILE_PATTERN)
            val matcher = pattern.matcher(mobileNumber)
            return matcher.matches()
        }

        fun isValidEmail(email: String?): Boolean {
            return if (TextUtils.isEmpty(email)) false else Patterns.EMAIL_ADDRESS.matcher(email)
                .matches()
        }

        fun showAlert(context: Context, message: String?, title: String?) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton(
                R.string.ok,
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
            builder.create().show()
        }

        fun showCustomAlert(context: Context, message: String?, title: String?, type: Int) {
            PromptDialog(context)
                .setDialogType(type)
                .setAnimationEnable(true)
                .setTitleText(title)
                .setContentText(message)
                .setPositiveListener("OK", object : PromptDialog.OnPositiveListener {
                    override fun onClick(dialog: PromptDialog) {
                        dialog.dismiss()
                    }
                }).show()
        }

        fun makeLinks(
            textView: TextView,
            links: Array<String>,
            clickableSpans: Array<ClickableSpan?>
        ) {
            val spannableString = SpannableString(textView.text)
            for (i in links.indices) {
                val clickableSpan = clickableSpans[i]
                val link = links[i]
                val startIndexOfLink = textView.text.toString().indexOf(link)
                spannableString.setSpan(
                    clickableSpan, startIndexOfLink, startIndexOfLink + link.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            textView.movementMethod = LinkMovementMethod.getInstance()
            textView.setText(spannableString, TextView.BufferType.SPANNABLE)
        }

        fun showSnackbar(anchor: View?, message: String?) {
            anchor?.let {
                message?.let { it1 ->
                    Snackbar.make(
                        it,
                        it1, Snackbar.LENGTH_LONG
                    ).show()
                }
            }
        }

    }
}